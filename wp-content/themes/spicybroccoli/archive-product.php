<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel">
		<div class="wrapper cf">
			<div class="PageContent">

				<?php do_action('notifications'); ?>
			
				<div class="PageContent--title">
					<h1>Services</h1>
					<p>These are the services available for the location you have selected. Add a service to your cart using the buttons below.</p>
				</div><!-- .PageContent--title -->

				<div class="PageContent--Location">
					<p>To update your location choose another city below.</p>
					<?php GS_Session::city_form(); ?>
				</div><!-- .PageContent--Location -->

				<?php 
				// Get all the Product Categories and iterate over them
				$categories = get_terms('product_categories');

				?>

				<div class="PageContent--Packages PageContent--Packages__Services cf">
					<?php foreach($categories as $category): ?>
						<div class="PageContent--Packages--Item PageContent--Packages--Item__Services cf">
							<h3><?php echo $category->name; ?></h3>
							<?php 
							$args = array(
								'post_type' => 'product',
								'posts_per_page' => -1,
								'product_categories' => $category->slug
							);

							if ( GS_Session::get_city() ) {
								$id =  get_page_by_path( GS_Session::get_city(), $output, 'location');
								$args['connected_type'] = 'product_to_location';
								$args['connected_items'] = $id->ID;
								$args['connected_direction'] = 'to';
							}
							
							$service_items = new WP_Query($args);


							?>

							<?php if ($service_items): ?>

							<?php while($service_items->have_posts()): $service_items->the_post(); ?>

								<?php
								global $session_cart;
								$cart = $session_cart['cart_data'];
								$itemInCart = false;

								if (!$cart) {
									$cart = array();
								}
								
								if (count($cart) > 0) {
									$cartItem = array_values(array_filter($cart, function($cartItem){
										return ($cartItem['id'] == get_the_ID());
									}));
									if (count($cartItem) > 0) {
										$itemInCart = true;
									}

								}
								$action = ($itemInCart) ? 'removeFromCart' : 'addToCart';

								?>


								<div class="ItemContent ItemContent__Services cf">
									<span class="price">
										$<?php echo p2p_get_meta( $post->p2p_id, 'cost', true ); ?>
									</span>
									<?php the_post_thumbnail(); ?>
									<div class="ItemContent--Copy">
										<h4><?php the_title(); ?></h4>
										<?php the_excerpt(); ?>
										<a href="<?php echo add_query_arg(array($action => 1, 'service_id' => get_the_ID())); ?>" class="AddToWishlist button"><span class="fa fa-shopping-cart"></span> <?php echo ($itemInCart) ? 'Remove From Cart' : 'Add To Cart'; ?></a>
									</div><!-- .ItemContent--Copy -->
								</div><!-- .ItemContent -->
							<?php endwhile; ?>
						<?php else: ?>
							<p>Sorry services are available in your area</p>
						<?php endif; ?>
						</div><!-- .PageContent--Packages--Item -->
					<?php endforeach; ?>
				</div><!-- .PageContent--Packages -->
				
			</div><!-- .PageContent -->
		</div><!-- .wrapper -->
	</section><!-- .panel -->

	<?php GS_Session::city_form_modal(); ?>

<?php get_footer(); ?>