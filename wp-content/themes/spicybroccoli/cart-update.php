<?php

define('COOKIE_KEY', 'gs_session_cookie'); 
define('TRANSIENT_KEY', 'gs_'); // Need to keep transient key under 45 chars


if ($_POST['city']) {



    global $session_cart;

    $session_data['city'] = $_POST['city'];
    $session_data['cart_data'] = array();
    
    $transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
    
    $new_data = set_transient( $transient_key, $session_data, 3 * DAY_IN_SECONDS );

    //exit;
    wp_redirect($_POST['referrer']);
}