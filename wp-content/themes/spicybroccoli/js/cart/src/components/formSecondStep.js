 var React = require('react');
 var CartItemActions = require('../actions/CartItemActions');
 var CartItemStore = require('../stores/CartItemsStore');
 var DOMPurify = require('dompurify');

 var SecondStep = React.createClass({
   getInitialState: function() {
     return {
       serviceCategory: null,
       step: 1
     }
   },
   componentWillReceiveProps: function(nextProps) {
     
   
     if (nextProps.city !== this.props.city) {
      //CartItemActions.clearCart();
      this.setState({
        step: 1
      });
     }
   
   },
   servicesFilteredByLocation: function() {
     return window.services.filter(function(service){
       return (!!~service.location.indexOf(this.props.city))
     }, this);
   },
   serviceCategories: function() {
     return window.services.map(function(service) {
       return service.serviceCategory;
     }).reduce(function(carry, cur){
       return carry.concat(cur);
     }, []).filter(function(value, index, self) {
       return self.indexOf(value) === index;
     })

   },
   servicesFilteredByServiceCategory: function(serviceCategory) {
     return window.services.filter(function(service){
       
       return (!!~service.serviceCategory.indexOf(serviceCategory));
     })
   },
   onClick: function(e) {
 
     this.setState({
       serviceCategory: e.target.textContent,
       step: 2
     });
   },
   render: function() {
 
     var serviceCategories = this.serviceCategories().map(function(service, index){
        var cssClass = ( this.state.serviceCategory == service ) ? 'active' : '';
       return <li className={cssClass} onClick={this.onClick} key={index} val={service}>{service}</li>
     }, this);

     var services = this.servicesFilteredByServiceCategory(this.state.serviceCategory);

     var modelsFilteredByLocation = window.models.filter(function(model) {
       return (!!~model.location.indexOf(this.props.city))
     }, this);

     var ServicesItem = (this.state.step > 1) ? <div><ServicesList cartItems={this.props.cartItems} services={services} city={this.props.city} /></div> : null;
   
     return (
       <div className="PageContent--BookingProcess-step StepTwo">
        <div className="PageContent--BookingProcess-step-headline">
          <span>Step Two</span>
          <div>Choose Services</div>
        </div> 
         <ul className="service-cat cf">
           {serviceCategories}
         </ul>
         {ServicesItem}
       </div>
     )
   }
 });


  var ServicesList = React.createClass({
        getInitialState: function() {
          return {
            currentSelection: null
          }
        },
        onClick: function(e) {
         
          this.setState({
            currentSelection: e.target.textContent
          })
        },
        render: function() {

          var services = this.props.services.map(function(service){
            var selected = ( this.state.currentSelection == service.name ) ? 'selected' : '';
            return <Service key={service.id} cartItems={this.props.cartItems} onClick={this.onClick} city={this.props.city} selected={selected} service={service} />;
          }, this);
          return <ul className="service">{services}</ul>
        }
      })

      var Service = React.createClass({
        addModel: function(model, service) {
          CartItemActions.modelAdd({model: model.id, service: service.id});      
        },
        serviceToggle: function() {
          var action = this.serviceInCart() ? 'Remove' : 'Add'; 
          CartItemActions['service'+action]({id: this.props.service.id});
        },
        serviceInCart: function() {
            return this.props.cartItems.some(function(cartItem){
                return cartItem.id == this.props.service.id
            }, this)
        },
        render: function() {
          var Models = (this.props.selected) ? <div><ModelList cartItems={this.props.cartItems} onClick={this.addModel} city={this.props.city} service={this.props.service} /></div> : null;
          var liClass = (this.props.selected) ? 'section active' : 'section';
          var isAdded = false;
  
          return (
            <li className={liClass}>
            <div onClick={this.props.onClick} className="service-title">{this.props.service.name}</div>
            {
              (this.props.selected)
              ? <div className="service-content">
                <button onClick={ this.serviceToggle } className="addCart">{ !this.serviceInCart() ? "Add To Cart" : "Remove from Cart"}</button>
                <div dangerouslySetInnerHTML={{__html: DOMPurify.sanitize(this.props.service.description)}} />
                {Models}
                </div>
              : null
            }
            </li>
            )
        }
      })

      var ModelList = React.createClass({
        //
        ModelsFilteredByLocationAndService: function() {
          return window.models.filter(function(model){
            return (!!~model.location.indexOf(this.props.city))
          }, this).filter(function(model){
            return (!!~model.services.indexOf(this.props.service.name))
          }, this)
        },
        onClick: function(model) {
          this.props.onClick(model, this.props.service);
        },
        render: function() {

          var models = this.ModelsFilteredByLocationAndService().map(function(model){
            return <Model onClick={this.onClick} cartItems={this.props.cartItems} service={this.props.service} model={model} key={model.id}/>
          }, this);
          return (
            <ul className="model cf">
            {models}
            </ul>
            )
        }
      })

      var Model = React.createClass({
        getInitialState: function() {
          return {
            selected: false
          }
        },
        toggleModel: function() {
          var action = this.isModelSelected() ? 'Remove' : 'Add'; 
          CartItemActions['model'+action]({service: this.props.service.id, model: this.props.model.id});
        },
        checkNested: function(obj) {
          var args = Array.prototype.slice.call(arguments, 1);

          for (var i = 0; i < args.length; i++) {
            if (!obj || !obj.hasOwnProperty(args[i])) {
              return false;
            }
            obj = obj[args[i]];
          }
          return true;
        },
        isModelSelected: function() {
          return this.props.cartItems.some(function(cartItem){
            if (cartItem.id == this.props.service.id) {
              if (cartItem.models) {
                return cartItem.models.some(function(model){
                  return (model.id == this.props.model.id)
                }, this)
              } else {  
                return false;
              }
            } else {
              return false;
            }
            
          }, this)
        },
        render: function() {
        

          var cssClass = ( this.isModelSelected() ) ? 'added' : '';
          
          return (
            <li onClick={this.toggleModel} className={cssClass}>
              <div className="addToWishlist">Add</div>
              <img src={this.props.model.image} />
              <span className="model-name">{this.props.model.name}</span>
            </li>
          )
        }
      })


      
  module.exports = SecondStep;