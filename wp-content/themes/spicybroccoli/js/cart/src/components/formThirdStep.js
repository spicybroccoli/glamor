var React = require('react');
var Reflux = require('reflux');
var CartStore = require('../stores/CartStore');

var CheckoutActions = React.createClass({
	mixins: [Reflux.connect(CartStore,"cart")],
	render: function() {
		var total = CartStore.getTotal();
		var Checkout = (this.state.cart.length > 0)
		? <div>
				<form action="checkout?type=enquiry" method="post">
					<input type="hidden" name="totalValue" value={ total } />
					<input type="hidden" name="formData" value={ JSON.stringify(this.state.cart) } />
					<input type="submit" name="submission_type" value="Send an Enquiry" />
				</form>
				<form action="checkout?type=deposit" method="post">
					<input type="hidden" name="totalValue" value={ total } />
					<input type="hidden" name="formData" value={ JSON.stringify(this.state.cart) } />
					<input type="submit" name="submission_type" value="Pay a Deposit" />
				</form>
				<form action="checkout?type=payment" method="post">
					<input type="hidden" name="totalValue" value={ total } />
					<input type="hidden" name="formData" value={ JSON.stringify(this.state.cart) } />
					<input type="submit" name="submission_type" value="Pay Full Amount" />
				</form>

			</div>
		: null;
			
		return <div>{Checkout}</div>

	}
});

module.exports = CheckoutActions;