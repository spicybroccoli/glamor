var React = require('react');
var Reflux = require('reflux');
var CartItemStore = require('../stores/CartItemsStore');
var CartItemActions = require('../actions/CartItemActions');

var Cart = React.createClass({
  mixins: [Reflux.connect(CartItemStore,"cartItems")],
  getCartTotal: function() {
    // Calculate total of cart.
    return this.state.cartItems.reduce(function(carry, cur){
      return parseFloat(carry) + parseFloat(cur.subtotal)
    }, 0)
  },
  getGST: function() {
    var gst = 1.10;
    return parseFloat( this.getCartTotal() - (this.getCartTotal() / parseFloat(gst) ) ).toFixed(2);
  },
  getDeposit: function() {
    return this.state.cartItems.reduce(function(carry, cur){
      console.log(cur.deposit[window.currentCity]);
      return parseFloat(carry) + parseFloat(cur.deposit[window.currentCity])
    }, 0)
  },
  onDurationUpdate: function(duration, service) {
    CartItemActions.durationUpdate({duration: duration, service: service.id});    
  },
  onCountUpdate: function(qty, service) {
    CartItemActions.qtyUpdate({qty: qty, service: service.id});    
  },
  onRemoveCartItem: function(key) {
    CartItemActions.serviceRemove({id:key});
  },
  render: function() {
    console.log(this.getCartTotal())
    var gstTotals = (this.getCartTotal() > 0) ? <CartSum label="GST (10%)" value={this.getGST()} /> : null;
          
    var depositTotals = (this.getCartTotal() > 0) ? <CartSum label="Deposit" value={this.getDeposit()} /> : null;

    var cartTotals = (this.getCartTotal() > 0) ? <CartSum label="Total" value={this.getCartTotal()} /> : null;

    return (
      <div>
        {
          (this.getCartTotal() > 0)
          ? null
          : <p>Your Cart is empty</p>
        }
        {
          this.state.cartItems.map(function(cartItem) {
            return <CartItem key={cartItem.id} onRemoveCartItem={this.onRemoveCartItem} cartItem={cartItem} onCountUpdate={this.onCountUpdate} onDurationUpdate={this.onDurationUpdate} />
          }, this)
        }

        <div className="CartTotal">      
          {depositTotals}
          {gstTotals}
          {cartTotals}
        </div>

      </div>
    )
  }

})

var CartItem = React.createClass({
  removeItem:function() {
    this.props.onRemoveCartItem(this.props.cartItem.id);
  },
  render: function() {
    var wishlist = (this.props.cartItem.models)
    ? <div className="CartItem--wishlist">
        <div className="CartItem--wishlist-title"><span className="fa fa-heart"></span>Wishlist</div>
        <ul className="wishlist">
         { 
          this.props.cartItem.models.map(function(model){
            return <li key={model.id}>{model.name}</li>
          })
          }
        </ul>
      </div>
    : null;
    return (
      <div className="CartItem">
        <div className="CartItem--title">{this.props.cartItem.name}</div>
        <div onClick={this.removeItem} className="CartItem--remove">&times;</div>
        <div className="CartItem--amount">
          Qty: <SelectWidget change={this.props.onCountUpdate} cartItem={this.props.cartItem} min="1" max="9" selected={this.props.cartItem.qty} />
          {
            (this.props.cartItem.pricingType == 'Variable') 
            ? <div>{ (this.props.cartItem.partyPackage) ? "PP:" : "Hrs:" }<SelectWidget change={this.props.onDurationUpdate} cartItem={this.props.cartItem} max={this.props.cartItem.maxDuration} min={this.props.cartItem.minDuration} selected={this.props.cartItem.duration}/></div>
            : null
          }
          <div>
            <span className="subtotal">
            ${ this.props.cartItem.subtotal }
            </span>
          </div>
        </div>
        {wishlist}
      </div>
    )
  }
});

var CartSum = React.createClass({
  render: function() {
    return (
      <div className="CartSum">
        <span>{this.props.label}</span>
        <span className="value">      
          <span>${this.props.value}</span>
        </span>
      </div>
      )
  }
})

var SelectWidget = React.createClass({
  onClick: function(e) {
    this.props.change(e.target.value, this.props.cartItem);
  },
  render: function() {
    var listItems = [];

    for (var i = parseInt(this.props.min); i <= parseInt(this.props.max); i++) {
      listItems.push(<option key={i} value={i}>{i}</option>)
    }

    return (
      <select onChange={this.onClick} value={this.props.selected}>
        {listItems}
      </select> 
    )
  }
})

module.exports = Cart;