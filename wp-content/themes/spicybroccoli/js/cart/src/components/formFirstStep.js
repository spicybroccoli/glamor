var React = require('react');
var CartItemActions = require('../actions/CartItemActions');

var FirstStep = React.createClass({
  componentDidMount: function() {
    if (this.props.city) {
      this.props.next({city: this.props.city, step: 2});
    }
  },
  onChange: function(e) {
    e.preventDefault();

    if (this.props.city) {
      if (e.target.value != this.props.city) {
        var change = confirm('Are you sure? Your cart will be cleared');
        if (change) {
          CartItemActions.cartClear()
          if (e.target.value == -1) {
            window.currentCity = null;
            this.props.next({city: null, step: 1})
          } else {
            window.currentCity = e.target.value;
            this.props.next({city: e.target.value, step: 2})
          }
        }
      }
    } else {
      CartItemActions.cartClear()
      window.currentCity = e.target.value;
      this.props.next({city: e.target.value, step: 2})
    }

  },
  render: function() {
    var citiesItems = [<option value="-1" key="-1">--</option>].concat(this.props.cities.map(function(city){
      return <option key={city.id} value={city.name}>{city.name}</option>
    }));

    
    return (
      <div className="PageContent--BookingProcess-step StepOne">
        <div className="PageContent--BookingProcess-step-headline">
            <span>Step One</span>
            <div>Choose Location</div>
        </div>
        <select onChange={this.onChange} value={(this.props.city) ? this.props.city : null}>
          {citiesItems}
        </select>
      </div>
    )
  }
});

module.exports = FirstStep