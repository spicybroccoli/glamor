var Reflux = require('reflux');
    'use strict';

    // Each action is like an event channel for one specific event. Actions are called by components.
    // The store is listening to all actions, and the components in turn are listening to the store.
    // Thus the flow is: User interaction -> component calls action -> store reacts and triggers -> components update

    var CartActions = Reflux.createActions([
        "orderClear",
        "qtyUpdate",
        "durationUpdate",
        "updateOrders"        // called by hitting enter in field in TodoHeader
    ]);

    module.exports = CartActions;