var Reflux = require('reflux');
var React = require('react');

var CartItemStore = require('./stores/CartItemsStore');

var CitySelect = require('./components/formFirstStep');
var ServiceSelect = require('./components/formSecondStep');
//var CheckoutActions = require('./components/formThirdStep');
var Cart = require('./components/cartTable');

var BookingSelection = React.createClass({
  mixins: [Reflux.connect(CartItemStore,"cartItems")],
  getInitialState: function() {
    var city = (window.currentCity) ? window.currentCity : null;
    return {
      step: 1,
      city: city
    }
  },
  next: function(data) {
    var newState = this.state;
    if (data) {
      for (var attrname in data) { 
        newState[attrname] = data[attrname]; 
      }
    }
    this.setState(newState);
  },
  render: function() {
   
    var SteppedForm = [];

    if (this.state.step >= 1) {
      SteppedForm.push(<CitySelect key="1" cities={cities} next={this.next} city={this.state.city} />);
    }
    if (this.state.step >= 2) {
      SteppedForm.push(<ServiceSelect key="2" cartItems={this.state.cartItems} next={this.next} city={this.state.city} />);
    }
    return <div>{SteppedForm}</div>        
  }

});

React.render(<BookingSelection />, document.getElementById('checkout'));

React.render(<Cart />, document.getElementById('cartContents'));

//React.render(<CheckoutActions />, document.getElementById('checkout-actions'));