/* var browserify = require('browserify');
var gulp = require('gulp');
var source = require("vinyl-source-stream");
var reactify = require('reactify');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var watchify = require('watchify');
var gutil = require('gulp-util');
var livereload = require('gulp-livereload');

gulp.task('build-dev', function(){
  var b = browserify({debug: true});
  b.transform(reactify); // use the reactify transform
  b.add('./Cart.js');
  return b.bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('../'));
});

gulp.task('build', ['build-dev'], function(){
  var b = browserify();
  b.transform(reactify); // use the reactify transform
  b.add('./Cart.js');
  return b.bundle()
    .pipe(source('bundle.min.js'))
    .pipe(buffer()) // <----- convert from streaming to buffered vinyl file object
    .pipe(uglify()) 
    .pipe(gulp.dest('../'));
});

var bundler = watchify(browserify('./Cart.js', watchify.args));
// add any other browserify options or transforms here
bundler.transform(reactify);

gulp.task('js', function(){
  livereload.listen(35729);
  bundle();

}); // so you can run `gulp js` to build the file
bundler.on('update', bundle); // on any dep update, runs the bundler
bundler.on('log', gutil.log); // output build logs to terminal

function bundle() {
  return bundler.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    // optional, remove if you dont want sourcemaps
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
      .pipe(sourcemaps.write('./')) // writes .map file
    //

    .pipe(gulp.dest('../'))
    .pipe(livereload({start: true}));
} */



var gulp = require('gulp');
var util = require('gulp-util');
var watchify = require('watchify');
var reactify = require('reactify');
var browserify = require('browserify');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

var notifier = require('node-notifier');

gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: "../../"
        }    
    })
});

gulp.task('watch', function () {
    var bundler = watchify(browserify('./Cart.js', watchify.args))
        .on('update', function () { util.log('Rebundling...'); })
        .on('time', function (time) {
            util.log('Rebundled in:', util.colors.cyan(time + 'ms'));
        });

    bundler.transform(reactify);
    bundler.on('update', rebundle);

    function rebundle() {
        return bundler.bundle()
            .on('error', function (err) {
                util.log(err);
                notifier.notify({ title: 'Browserify Error', message: 'Something went wrong :/' });
            })
            .pipe(source('bundle.js'))
            .pipe(buffer())
            .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
            .pipe(uglify())
            .pipe(sourcemaps.write('./')) // writes .map file
            .pipe(gulp.dest('../'))
            .pipe(browserSync.reload({ stream: true }));
    }

    return rebundle();
});

gulp.task('default', ['watch', 'browser-sync']);