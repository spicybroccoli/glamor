var Reflux = require('reflux');
var CartListActions = require('../actions/CartItemActions');
var assign = require('object.assign');
var request = require('superagent')
    'use strict';

    var _cartItems = [];

    var ajaxQueue = (function(){
        var queue = [];
        var isRunning = false;

        var add = function(payload) {
            // signature { data: data, callback: callback }
            queue.push(payload)
        }

        var start = function() {
            if (!isRunning) {
                current = queue.shift()
                if (typeof current != 'undefined') {
                    request.post('/glamor/wp-admin/admin-ajax.php?action=sync')
                    .send(current.data)
                    .end(function(err, res){
                        if (err) callback(err, null);
                        current.callback(null, res)
                        start();
                    })
                }
            }
        }

        return {
            add: add,
            start: start
        }
    })()


    defaultCartItem = { 
        models: null, 
        qty: null,
        deposit: null,
        duration: null,
        subtotal: null,
    }


    var cartListStore = Reflux.createStore({
        // this will set up listeners to all publishers in TodoActions, using onKeyname (or keyname) as callbacks
        listenables: [CartListActions],
        onServiceAdd: function(payload) {
            // signature { id: serviceID }

            if (!this.serviceInCart(payload.id)) {
                var service = window.services.filter(function(service){
                    return service.id == payload.id;
                })

                var service = assign({}, defaultCartItem, service[0])

                this.updateCart(_cartItems.concat([service]));
            }

        },
        onCartClear: function() {
            this.updateCart([]);
        },
        onServiceRemove: function(payload) {
             // signature { id: serviceID }
            if (this.serviceInCart(payload.id)) {
                _cartItems = _cartItems.filter(function(cartItem){
                    return (cartItem.id != payload.id)
                })
                this.updateCart(_cartItems)
            }
        },
        onModelAdd: function(payload) {
            // signature { model: modelID, service: serviceID }

            var model = window.models.filter(function(model){
                return model.id == payload.model
            })[0];

            console.log('Payload: ', payload)

            // Add the service if it doesnt exist
            if (!this.serviceInCart(payload.service)) {
                var service = window.services.filter(function(service){
                    return service.id == payload.service;
                })

                // Add default null values (to prevent TypeError in render)
                var service = assign({}, defaultCartItem, service[0])
    
                _cartItems = _cartItems.concat([service])
            }
            
            _cartItems = _cartItems.map(function(cartItem){
                if (cartItem.id == payload.service) {
                    if (cartItem.models) {
                        var modelExists = cartItem.models.some(function(serviceModel){
                            return (serviceModel.id == model.id)
                        })
                        if (!modelExists) {
                            cartItem.models = cartItem.models.concat([model]);
                        }
                    } else {
                        cartItem.models = [model];
                    }
                }
                return cartItem
            })
            
            this.updateCart(_cartItems);
     
        },
        onModelRemove: function(payload) {
            // signature { model: modelID, service: serviceID }
            if (this.serviceInCart(payload.service)) {
                _cartItems = _cartItems.map(function(cartItem){
                    if (cartItem.id == payload.service) {
                        if (cartItem.models) {
                            var modelExists = cartItem.models.some(function(serviceModel){
                                return (serviceModel.id == payload.model)
                            })
                            if (modelExists) {
                                cartItem.models = cartItem.models.filter(function(model){
                                    return (model.id != payload.model)
                                })
                                if (!cartItem.models.length) {
                                    cartItem.models = null;
                                }
                                console.log('modelRemove - cartItem: ', cartItem)
                            }
                        }
                    }
                    return cartItem;
                })

                console.log('modelRemove - cartItems: ', _cartItems)
                this.updateCart(_cartItems);
            } 
        },
        onQtyUpdate: function(payload) {
            // signature { qty: quantity, service: serviceID }
            if (this.serviceInCart(payload.service)) {
                _cartItems = _cartItems.map(function(cartItem){
                    if (cartItem.id == payload.service) {
                        cartItem.qty = payload.qty
                    }
                    return cartItem;
                })
                this.updateCart(_cartItems);
            }

        },
        serviceInCart: function(id) {
            return _cartItems.some(function(cartItem){
                return cartItem.id == id
            })
        },
        onDurationUpdate: function(payload) {
            // signature { duration: duration, service: serviceID }
            if (this.serviceInCart(payload.service)) {
                _cartItems = _cartItems.map(function(cartItem){
                    if (cartItem.id == payload.service) {
                        cartItem.duration = payload.duration
                    }
                    return cartItem;
                })
                this.updateCart(_cartItems);
            }

        },
        updateCart: function(cartItems){

            

            // if we used a real database, we would likely do the below in a callback
            _cartItems = cartItems;

            _cartItems = _cartItems.map(function(cartItem){

                var qty = cartItem.qty || 1;
                var duration = cartItem.duration || cartItem.minDuration

                cartItem.qty = qty;
                cartItem.duration = duration
                cartItem.subtotal = parseFloat( cartItem.qty * cartItem.cost[window.currentCity] * cartItem.duration).toFixed(2);  

                return cartItem;
            })

            ajaxQueue.add({data: { cartItems: _cartItems, city: window.currentCity }, callback: function(err, res){
                console.log('Sync complete');
                console.log(res);
            }})
            ajaxQueue.start();

                       
            
            this.trigger(_cartItems); // sends the updated list to all listening components (TodoApp)
        },
        // this will be called by all listening components as they register their listeners
        getInitialState: function() {
           
            _cartItems = window.initialCart;

            //this.trigger(_cartItems);
            
            return _cartItems;

        }
    });

module.exports = cartListStore;