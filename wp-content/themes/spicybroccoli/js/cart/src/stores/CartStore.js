var Reflux = require('reflux');
var CartActions = require('../actions/CartActions');

    'use strict';

    var _orders = [];
    var _orderQty = [];
    var _orderDuration = [];
    var _cartItems = []

    var cartStore = Reflux.createStore({
        // this will set up listeners to all publishers in TodoActions, using onKeyname (or keyname) as callbacks
        listenables: [CartActions],
        onUpdateOrders: function(cartItems) {
            _cartItems = cartItems;
            this.updateCart(this.getOrders(_cartItems));
        },
        onOrderClear: function() {
            _orders = _orderQty = _orderDuration = [];
            this.updateCart(_orders)
        },
        onQtyUpdate: function(payload) {

            var service = payload.service;
            var data = payload.data;            

            var serviceExists = _orderQty.some(function(item){
              return (item.name == service.name)
            });

            var newQty;


            if (serviceExists) {
              // We're updating qty state
              newQty = _orderQty.filter(function(item){
                return (item.name !== service.name)
              }).concat([{ name: service.name, qty: data }]);
            } else {
              newQty = _orderQty.concat([{ name: service.name, qty: data }]);
            }

            _orderQty = newQty;
            this.updateCart(this.getOrders(_cartItems));
        },
        onDurationUpdate: function(payload) {

            var service = payload.service;
            var data = payload.data;  

            var serviceExists = _orderDuration.some(function(item){
              return (item.name == service.name)
            });

            var newDuration;

            if (serviceExists) {

              newDuration = _orderDuration.filter(function(item){
                return (item.name !== service.name)
              }).concat([{ name: service.name, duration: data }]);
              
            } else {
              newDuration = _orderDuration.concat([{ name: service.name, duration: data }]);
            }

            _orderDuration = newDuration;
            this.updateCart(this.getOrders(_cartItems));
        },
        // called whenever we change a list. normally this would mean a database API call
        getOrders: function(cartItems) {
           


            var getUniqueServices = function() {
                var services = [];
                return cartItems.map(function(arrItem){
                   
                    return {service: arrItem.service, city: arrItem.city}
                }).filter(function(item){
                   
                    var itemExists = (!!~services.indexOf(item.service.name));
                    services.push(item.service.name);
                    return !itemExists;
                 })
            }.bind(this);

            var getGroupedServices = function(service) {
                return cartItems.filter(function(cartItem){
                    return ( cartItem.service.name == service );
                })
            }.bind(this)


            var orders = getUniqueServices().map(function(item){
    
                   var models = getGroupedServices(item.service.name).filter(function(cartItem){
                            return (cartItem.model != null)
                        }).map(function(cartItem){
                            console.log('cartItem: ',cartItem)
                            return cartItem.model.name
                        }).filter(function(value, index, self) {
                            return self.indexOf(value) === index;
                        }).reduce(function(carry, model){
                            carry.push(model)
                            return carry;
                        }, []);
                

                var qty = _orderQty.filter(function(qtyItem) {
                    return (item.service.name == qtyItem.name)
                });

                var qty = (!!qty.length) ? qty[0].qty : 1;  

                
                var duration = 1;

                if (item.service.pricingType == 'Variable') {
                    var duration = _orderDuration.filter(function(durationItem) {
                        return (item.service.name == durationItem.name)
                    });

                    var duration = (!!duration.length) ? duration[0].duration : item.service.minDuration;  
                }

                var subtotal = parseFloat( qty * item.service.cost[item.city] * duration).toFixed(2);

                return {
                    id: item.service.id,
                    name: item.service.name, 
                    item: item.service.name, 
                    pricingType: item.service.pricingType,
                    unitCost: item.service.cost[item.city], 
                    models: models, 
                    qty: qty,
                    partyPackage: item.service.partyPackage,
                    subtotal: subtotal, 
                    duration: { 
                        min: item.service.minDuration, 
                        max: item.service.maxDuration, 
                        value: duration 
                    } 
                };

            }, this);

            return orders;
        },
        getTotal: function() {
            return _orders.reduce(function(carry, order){
              var itemDur = order.duration.value || 1;
              return parseFloat(carry) + ( parseFloat(order.qty) * parseFloat(order.unitCost) * itemDur )
            }, 0)
        },
        updateCart: function(orders){
            
            // if we used a real database, we would likely do the below in a callback
            //_orders = this.getOrders(orders);
            this.trigger(orders); // sends the updated list to all listening components (TodoApp)
        },
        // this will be called by all listening components as they register their listeners
        getInitialState: function() {
            return _orders;
        }
    });

module.exports = cartStore;