var spicybroccoli = {};

spicybroccoli.init = function() {
	var cart;

	jQuery('.session-city select').on('change', function(e){

		var value = this.value;

		if (!!window.currentCity) {
			if (value != window.currentCity) {
			  var change = confirm('Are you sure? Your cart will be cleared');
			  if (change) {

			  	var $form = jQuery(this).parents('form');
			  	
			  	$form.find('option').removeAttr('selected')
			  	$form.find('option[value="'+value+'"]').attr('selected', 'selected');
			  	$form.find('select').val(value);

			  	$form.submit();

			  } else {
			  	return false;
			  }
			}

		} else {
		
			var $form = jQuery(this).parents('form');
			
			$form.find('option').removeAttr('selected')
			$form.find('option[value="'+value+'"]').attr('selected', 'selected');
			$form.find('select').val(value);

			console.log($form.find('select').val());
			$form.submit();
		}
	



		//jQuery('form.session-city').submit()
	})

	if (!!jQuery('.formWrapper form').length) {
		jQuery('.formWrapper form').parsley().subscribe('parsley:form:success', function(){

			jQuery('.cart-loader').addClass('show');
		})
	}

	jQuery('#eventDateTime').DateTimePicker();



}


// Run it
jQuery(spicybroccoli.init);

jQuery(window).load(function(){
	jQuery('.SiteHeader').css('height', jQuery('.SiteHeader .slides li').height())
	jQuery('.SiteHeader--hero .slides').css('height', jQuery('.SiteHeader .slides li').height())
	jQuery(window).on('resize', function(){
		jQuery('.SiteHeader').css('height', jQuery('.SiteHeader .slides li').height())
		jQuery('.SiteHeader--hero .slides').css('height', jQuery('.SiteHeader .slides li').height())
	})
})