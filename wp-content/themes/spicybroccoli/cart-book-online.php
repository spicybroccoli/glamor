<?php get_header(); ?>

  <section class="panel">
    <div class="wrapper cf">
      <div class="PageContent">
        <div class="PageContent--title">
          <h1>Book Online</h1>
          <p>Follow our simple 3 step process to secure your booking.</p>
        </div><!-- .PageContent--title -->
        <div class="PageContent--BookingProcess">
          <div class="BookingForm">
            <div id="checkout"></div>
            <div id="checkout-actions"></div>
          </div>
        </div>
      </div>
      <div class="PageContent--sidebar">
        <div class="PageContent--sidebar-cart">
        <div class="CartForm">
          <div class="PageContent--sidebar-cart-headline cf">
            <div class="PageContent--sidebar-cart-headline--text">Your Cart</div>
            <span class="fa fa-shopping-cart icon"></span>
          </div>
          <div id="cartContents"></div>
        </div>
        </div><!-- .PageContent--sidebar-cart -->
        <div id="cart-actions"></div>
      </div><!-- .PageContent--sidebar -->
    </div>
  </section>

<script src="<?php echo get_stylesheet_directory_uri();?>/js/cart/bundle.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function(){
  window.models.forEach(function(model){
    var img = new Image();
    img.src = model.image;
  });
});
</script>
<?php get_footer(); ?>