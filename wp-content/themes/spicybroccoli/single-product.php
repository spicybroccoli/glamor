<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="panel">
	<div class="wrapper cf">
	<div class="PageContent">
		
					<?php do_action('notifications'); ?>

		<div class="PageContent--content">

			<?php while(have_posts()): the_post(); ?>
			
				<?php
				global $session_cart;
				$cart = $session_cart['cart_data'];
				$itemInCart = false;

				if (!$cart) {
					$cart = array();
				}
				
				if (count($cart) > 0) {
					$cartItem = array_values(array_filter($cart, function($cartItem){
						return ($cartItem['id'] == get_the_ID());
					}));
					if (count($cartItem) > 0) {
						$itemInCart = true;
					}

				}
				$action = ($itemInCart) ? 'removeFromCart' : 'addToCart';

				?>

			<div class="PageContent--title">
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			</div><!-- .PageContent--title -->

			<div class="PageContent--SinglePackage cf">
				<div class="PageContent--SinglePackage--HeroImage">
						<?php
							$galleryArray = get_post_gallery_ids(get_the_ID()); 

						?>
						<?php foreach ($galleryArray as $id): ?>
						<div class="item">
							<a href="#"><img src="<?php echo wp_get_attachment_url( $id ); ?>" alt=""></a>
						</div>
						<?php endforeach; ?>
				</div><!-- .PageContent--SinglePackage--HeroImage -->

				<div class="PageContent--SinglePackage--LongDesc">
					<?php echo wpautop(get_post_meta(get_the_ID(), 'package-details', true)); ?>
				</div><!-- .PageContent--SinglePackage--LongDesc -->
				<div class="PageContent--SinglePackage--Gallery">
					<?php
						$galleryArray = get_post_gallery_ids(get_the_ID()); 

					?>
					<h3>Photo Gallery</h3>
					<ul class="GalleryList">
						<?php foreach ($galleryArray as $id): ?>
						<li class="item">
							<img src="<?php echo wp_get_attachment_thumb_url( $id ); ?>" alt=""/>
						</li>
						<?php endforeach; ?>
					</ul>
				</div><!-- .PageContent--SinglePackage--Gallery -->
				<div class="PageContent--SinglePackage--Cost">
					<?php echo wpautop(get_post_meta(get_the_ID(), 'package-cost-details', true)); ?>
					<a href="<?php echo add_query_arg(array($action => 1, 'service_id' => get_the_ID())); ?>" class="AddToWishlist button"><span class="fa fa-shopping-cart"></span> <?php echo ($itemInCart) ? 'Remove From Cart' : 'Add To Cart'; ?></a>
				</div><!-- .PageContent--SinglePackage--Cost -->
			</div><!-- .PageContent--SinglePackage -->
		<?php endwhile; ?>
			</div>
		</div><!-- .PageContent -->
	</div><!-- .wrapper -->
</section>

<section class="panel panel__dark">
	<?php get_template_part('partials/book-online'); ?>
</section>
<section class="panel">
	<?php get_template_part('partials/contact-us'); ?>
</section>

<script>

jQuery(document).ready(function($) {
 
  var sync1 = $(".PageContent--SinglePackage--HeroImage");
  var sync2 = $(".GalleryList");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:false,
    transitionStyle : "fade",
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 3,
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    sync2
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if(sync2.data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  sync2.on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
 
});
</script>

<?php get_footer(); ?>