<?php get_header(); ?>



<section class="panel panelCheckout">
<div class="wrapper cf">
        <?php 
        global $session_cart;
        $orders = $session_cart['cart_data'];

        if (!empty($orders)):

        
        if ( get_query_var('cart') == 'checkout' || get_query_var('cart') == 'enquiry' ) {

            $total = array_reduce($orders, function($carry, $item){
                $subtotal = (int) $item['subtotal'];
                $carry += $subtotal;
                return $carry;
            }, 0);

        } elseif ( get_query_var('cart') == 'pay-deposit' ) {

            $total = array_reduce($orders, function($carry, $item) use ($session_cart) {
                $subtotal = (int) $item['deposit'][$session_cart['city']];
                $carry += $subtotal;
                return $carry;
            }, 0);
        }


        $step = ( $_GET['step'] ) ? $_GET['step'] : 1;

       ?>

        <?php if ( get_query_var('cart') == 'checkout' || get_query_var('cart') == 'pay-deposit' ) { ?>
            <h1>Checkout</h1>
        <?php } elseif ( get_query_var('cart') == 'enquiry' ) { ?>
            <h1>Enquiry</h1>
            <?php } ?>

        <?php if ($step == 1): ?>

            <h3>Step 1 - Contact and Event Details</h3>
            <?php if ( get_query_var('cart') == 'checkout' || get_query_var('cart') == 'pay-deposit' ) { ?>
                <p>Please enter your contact and event details below.</p>
            <?php } ?>
            
            <?php include('inc/cart/cart_form-customer-details.php'); ?>


       <?php endif; ?>


        
        <?php if ($step == 2): ?>
        <?php

        if ( get_query_var('cart') == 'checkout' ) {

           include('inc/cart/cart_email-admin-total-pending.php');

        } elseif ( get_query_var('cart') == 'pay-deposit' ) {

            include('inc/cart/cart_email-admin-deposit-pending.php');

        } elseif ( get_query_var('cart') == 'enquiry' ) {

            include('inc/cart/cart_email-enquiry.php');
        }


        

        if ( get_query_var('cart') == 'checkout' ||  get_query_var('cart') == 'pay-deposit' ) {

            require('inc/lib/eWAY/RapidAPI.php');

            $redirect_url = site_url('/cart/checkout/process');

            $request = new eWAY\CreateAccessCodeRequest();
            $request->Payment->TotalAmount = $total*100; // Amount is in cents, 100 = $1.00
            $request->RedirectUrl = $redirect_url;
            $request->Payment->CurrencyCode = 'AUD';
            $request->Payment->InvoiceReference = $post_id;

            $request->Customer->FirstName = $_POST['firstName'];
            $request->Customer->LastName = $_POST['lastName'];

            $service = new eWAY\RapidAPI(
                '60CF3C6jTZkBVIfK5heEc8suXQssuyOddprG5Gn8qI0yLuzwVPMnojvhntU7RHnKpc7Qt6', 
                'D1nnert1me', 
                array('sandbox' => true)
            );

            $result = $service->CreateAccessCode($request);
        
        

        ?>

        <!-- to display errors returned by createToken -->
        <h3>Step 2 - Credit Card Details</h3>
        <p>Please review your order and confirm your payment details below.</p>
        
        <h4>Your Order</h4>
        <?php if ( get_query_var('cart') == 'checkout' ) {

           include('inc/cart/cart_table-order-total.php');

        } elseif ( get_query_var('cart') == 'pay-deposit' ) {

            include('inc/cart/cart_table-order-deposit.php');

        } 
        ?>


        <h4>Your Contact Details</h3>
        <table class="cart-checkout cart-contact responsive">
            <tr>
                <td>First Name:</td>
                <td><?php echo $_POST['firstName']; ?></td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><?php echo $_POST['lastName']; ?></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><?php echo $_POST['email']; ?></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td><?php echo $_POST['phone']; ?></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><?php echo $city; ?></td>
            </tr>
            <tr>
                <td>Event Address:</td>
                <td><?php echo $event_details['eventAddress']; ?></td>
            </tr>
            <tr>
                <td>Event Date/Time:</td>
                <td><?php echo $event_details['eventDateTime']; ?></td>
            </tr>
            <tr>
                <td>Message:</td>
                <td><?php echo $event_details['eventMessage']; ?></td>
            </tr>
        </table>

        <h4>Your Credit Card Details</h4>
        <div class="formWrapper">
            <div class="cart-loader">
                <img class="cart-loader--spinner" src="<?= site_url('/wp-content/themes/spicybroccoli/images/spinner.png'); ?>">
            </div>
        <form method="POST" class="payment-form credit-card cf" action="<?= $result->FormActionURL ?>" data-parsley-validate>
            <input type="hidden" name="EWAY_ACCESSCODE" value="<?= $result->AccessCode ?>" required />
            <input type="hidden" name="EWAY_PAYMENTTYPE" value="Credit Card" required />

            <div class="form-row">
                <label for="EWAY_CARDNAME">Cardholder Name</label>
                <input type="text" id="EWAY_CARDNAME" name="EWAY_CARDNAME" required/>
            </div>
                
            <div class="form-row">
                <label for="EWAY_CARDNUMBER">Card Number (5105105105105100)</label>
                <input type="text" id="EWAY_CARDNUMBER" name="EWAY_CARDNUMBER" required/>
            </div>

            <div class="form-row">
                <label>Expiry Date (MM/YY)</label>
                <div class="form-row--column">
                    <select name="EWAY_CARDEXPIRYMONTH" required>
                        <option>Month</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                </div>
                <div class="form-row--column">
                    <select name="EWAY_CARDEXPIRYYEAR" required >
                        <option>Year</option>
                        <option value="15">2015</option>
                        <option value="16">2016</option>
                        <option value="17">2017</option>
                        <option value="18">2018</option>
                        <option value="19">2019</option>
                        <option value="20">2020</option>
                    </select>
                </div>
            </div>

            <div class="form-row">
                <label>CVN</label>
                <input type="text" name="EWAY_CARDCVN" required />
            </div>

            <div class="form-row form-row-submit">
                <input type="submit" value="Confirm Payment" maxlength="3" size="3" text="Confirm Payment" />
            </div>
                
        </form>
        </div>
        <?php } else if (get_query_var('cart') == 'enquiry' ) { ?>
            <h3>Thanks for your enquiry!</h3>

            <h4>Your Enquiry</h4>
            <table class="cart-checkout responsive">
                <thead>
                    <tr>
                       <th>Service</th>
                       <th>Wishlist</th>
                       <th>Base Cost</th>
                       <th>Qty</th>
                       <th>Hrs / Pax</th>
                       <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($orders as $order): ?>
                        <tr>
                            <td><?= $order['name']; ?></td>
                            <td>
                                <?php
                                if ($order['models']) {
                                    $models = array_map(function($model) {
                                        return $model['name'];
                                    }, $order['models']);
                                    echo implode(', ', $models);
                                }
                                ?>
                            </td>
                            <td>$<?= $order['cost'][$session_cart['city']]; ?></td>
                            <td><?= $order['qty']; ?></td>
                            <td><?= $order['duration']; ?></td>
                            <td>$<?= $order['subtotal']; ?></td> 
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    </table>
                    <table class="cart-checkout">
                    <tbody>
                        <tr class="total">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td><strong>$<?= $total; ?></strong></td>
                        </tr>    
                </tbody>
            </table>

            <h4>Your Contact Details</h3>
            <table class="cart-checkout cart-contact responsive">
                <tr>
                    <td>First Name:</td>
                    <td><?php echo $_POST['firstName']; ?></td>
                </tr>
                <tr>
                    <td>Last Name:</td>
                    <td><?php echo $_POST['lastName']; ?></td>
                </tr>
                <tr>
                    <td>Email:</td>
                    <td><?php echo $_POST['email']; ?></td>
                </tr>
                <tr>
                    <td>Phone:</td>
                    <td><?php echo $_POST['phone']; ?></td>
                </tr>
                <tr>
                    <td>City:</td>
                    <td><?php echo $city; ?></td>
                </tr>
                <tr>
                    <td>Event Address:</td>
                    <td><?php echo $event_details['eventAddress']; ?></td>
                </tr>
                <tr>
                    <td>Event Date/Time:</td>
                    <td><?php echo $event_details['eventDateTime']; ?></td>
                </tr>
                <tr>
                    <td>Message:</td>
                    <td><?php echo $event_details['eventMessage']; ?></td>
                </tr>
            </table>
       <?php } ?>
        <?php endif; ?>

    <?php else: ?>
         <h1>Empty Cart</h1>
            <p>Your cart is empty.</p>
         <p>Follow our simple 3 step process to secure your booking.</p>
         <p><a class="button" style="margin:10px auto;" href="<?= site_url('/book-online'); ?>">Book Online</a></p>
    <?php endif; ?>

 
</div>
</section>

<?php get_footer(); ?>