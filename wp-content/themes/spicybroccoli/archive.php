<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<?php
		if ( is_front_page() ) {
    			// This is the mate slider
    			echo do_shortcode("[metaslider id=26]"); 
		} else {}
	?>
	
	<section class="panel blog blog-index">
		<div class="wrapper cf">
			<div class="PageContent--sidebar">
				<?php get_sidebar(); ?>
			</div>
			<div class="PageContent">
				<?php
				/* Run the loop to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-index.php and that will be used instead.
				 */
				 get_template_part( 'loop', 'index' );
				?>
			</div><!-- .PageContent -->
		</div><!-- .wrapper --> 
	</section><!-- .panel -->





	<!--<div class="wrapper blog">
		<div id="container">
			<div id="content" role="main">

			


			</div> #content 
		</div> #container 
	</div> .wrapper -->

<?php get_footer(); ?>