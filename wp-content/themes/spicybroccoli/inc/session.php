<?php

define('COOKIE_KEY', 'gs_session_cookie'); 
define('TRANSIENT_KEY', 'gs_'); // Need to keep transient key under 45 chars

add_action( 'init', function() {

    if ( isset( $_COOKIE[COOKIE_KEY] ) ) {
        $key = (string) $_COOKIE[COOKIE_KEY];

    } else {

        $key = md5( time() . rand() );

        // Set the browser cookie to expire in 30 minutes
        setcookie( COOKIE_KEY , $key, time() + 30 * 60, '/' );
    }

    // Try to grab the transient from the database, if it exists.
    $transient = get_transient( TRANSIENT_KEY . $key );

    // If the transient doesn't exist, create it.
    if ( false === $transient ) {
        // Call your function to generate the random number.
        $transient = array();

        // Store the transient, but expire in 30 minutes.
        set_transient( TRANSIENT_KEY . $key, $transient, 3 * DAY_IN_SECONDS );
    }

    $GLOBALS['session_cart'] = $transient;
});


add_action('wp_ajax_nopriv_sync', array('GS_Session' , 'session_sync'));
add_action('wp_ajax_sync', array('GS_Session' , 'session_sync'));

// Need to use wp_loaded because p2p_init hooks onto wp_loaded and not init.
// 
add_action('wp_loaded', array('GS_Session' , 'add_to_cart'));
add_action('notifications', array('GS_Session' , 'display_notifications'));


class GS_Session {
	static function get_city() {
		global $session_cart;
		return ($session_cart['city']) ? $session_cart['city'] : false;
	}

	static function display_notifications() {
		global $session_cart;
		$notifications = $session_cart['notifications'];
		if ($notifications) {
			$html = '<div class="Notifications">';
			if ($notifications['action'] == 'remove') {
				$action = 'removed from';
			} elseif ($notifications['action'] == 'add') {
				$action = 'added to';
			}

			if ($notifications['service']) {
				$service = get_post( $notifications['service']);
			}

			unset($session_cart['notifications']);
			$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
			
			$new_data = set_transient( $transient_key, $session_cart, 3 * DAY_IN_SECONDS );

			$html .= '<p><strong>' . $service->post_title . '</strong> was ' . $action . ' your cart</p>';
			$html .= '<a class="button" href="' . site_url('/cart'). '">View Cart</a>';
			$html .= '</div>';

			echo $html;
		}
	}

	static function add_to_cart() {
		if (array_key_exists('addToCart', $_GET)) {

			if (!GS_Session::get_city()) return;

			$model_id = filter_input(INPUT_GET, 'model_id', FILTER_VALIDATE_INT);
			$service_id = filter_input(INPUT_GET, 'service_id', FILTER_VALIDATE_INT);

			// Get Cart
			global $session_cart;
			$cart_data = $session_cart['cart_data'];

			if (count($cart_data) <= 0 || !$cart_data) {
				$cart_data = array();
			}

		

			// Get Model, CartItem collections
			 $models_query = new WP_Query(array('post_type' => 'model', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC'));
				$models = array(); 
				foreach ($models_query->posts as $modelInstance) {
				  $connected_services = new WP_Query( array(
				    'connected_type' => 'model_to_product',
				    'connected_items' => $modelInstance,
				    'nopaging' => true,
				  ) );

				  $connected_services = array_map(function($n){
				    return $n->post_title;
				  }, $connected_services->posts);

				  $connected_locations = new WP_Query( array(
				    'connected_type' => 'model_to_location',
				    'connected_items' => $modelInstance,
				    'nopaging' => true,
				  ) );

				  $connected_locations = array_map(function($n){
				    return $n->post_title;
				  }, $connected_locations->posts);

				  $thumb_id = get_post_thumbnail_id($modelInstance->ID);
				  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
				  $thumb_url = $thumb_url_array[0];

				  $modelsArray = array(
				    'id' => $modelInstance->ID,
				    'name' => $modelInstance->post_title,
				    'location' => $connected_locations,
				    'image' => $thumb_url,
				    'type' => getMetaValues(get_post_meta($modelInstance->ID, 'model-type', true)),
				    'services' => $connected_services,
				  );
				  $models[] = $modelsArray;
				}

			$services_query = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1));
			$services = array(); 
			foreach ($services_query->posts as $serviceInstance) {
			  $connected_prices = new WP_Query( array(
			    'connected_type' => 'product_to_location',
			    'connected_items' => $serviceInstance,
			    'nopaging' => true,
			  ) );



			  $locations = array_map(function($item){
			    return $item->post_title;
			  }, $connected_prices->posts);

			  $prices = array_reduce($connected_prices->posts, function($carry, $item){
			    $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'cost', true );
			    return $carry;
			  }, array());

			  $deposit = array_reduce($connected_prices->posts, function($carry, $item){
			    $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'deposit_value', true );
			    return $carry;
			  }, array());
			  
			  $servicesArray = array(
			    'id' => $serviceInstance->ID,
			    'name' => $serviceInstance->post_title,
			    'cost' => $prices,
			    'locations' => $locations,
			    'description' => wpautop($serviceInstance->post_content),
			    'pricingType' => get_post_meta($serviceInstance->ID, 'pricing-type', true),
			    'serviceCategory' => getMetaValues(get_post_meta($serviceInstance->ID, 'product-type', true)),
			    'minDuration' => (int) get_post_meta($serviceInstance->ID, 'min-duration', true),
			    'maxDuration' => (int) get_post_meta($serviceInstance->ID, 'max-duration', true),
			    'deposit' => $deposit,
			    'partyPackage' => (get_post_meta($serviceInstance->ID, 'party-package', true) == 'Yes') ? true : false
			  );
			  $services[] = $servicesArray;
			}

			// Check if service exists in cart
			$service_in_cart = array_values(array_filter($cart_data, function($cart_item) use ($service_id) {
				return $cart_item['id'] == $service_id;
			}));

		

			$model = array_values(array_filter($models, function($model_item) use ($model_id) {
				return $model_item['id'] == $model_id;
			}));

	


			// If Service does exist in cart, add the model to the service
			if (count($service_in_cart) > 0 ) {
				// Check if model was passed
				if ($model_id) {
					$currentService = $service_in_cart[0]; // Presume the filter will only return an array of length = 1.
					if ($currentService['models']) { // Check if null
						$model_in_service = array_values(array_filter($currentService['models'], function($service_model) use($model_id) {
							return $service_model['id'] == $model_id;
						}));
						// If model isn't already in service, add it
						if (count($model_in_service) <= 0 ) {
							$currentService['models'][] = $model[0];
						}
					} else {
						$currentService['models'] = array($model[0]);
					}

					// Update the cart
					$cart_data = array_map(function($cart_item) use ($service_id) {
						if ($cart_item['id'] == $service_id) {
							$cart_item = $currentService;
						}
						return $cart_item;
					}, array_values($cart_data));
				}

 
			} else {
				// If service does not exist, add it
				$service_to_add = array_values(array_filter($services, function($service) use ($service_id) {
					return $service['id'] == $service_id;
				}));


				// If this service exists
				if (count($service_to_add) > 0 ) {
					if ($model_id) {
						$service_to_add[0]['models'] = array($model[0]);
					}

					// defaults;
					$service_to_add[0]['qty'] = 1;
					$service_to_add[0]['duration'] = $service_to_add[0]['minDuration'];
					$service_to_add[0]['subtotal'] = (int) $service_to_add[0]['qty'] * (int) $service_to_add[0]['duration'] * (int) $service_to_add[0]['cost'][GS_Session::get_city()];

					// Add to cart
					$cart_data[] = $service_to_add[0];

				}

			}

			// Save the cart
			$session_cart['cart_data'] = $cart_data;
			$session_cart['notifications'] = array('action' => 'add', 'service' => $service_id, 'model' => $model_id);
			
			$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
			
			$new_data = set_transient( $transient_key, $session_cart, 3 * DAY_IN_SECONDS );

			$newUrl = remove_query_arg(array('addToCart', 'model_id', 'service_id'));

			wp_redirect($newUrl);
			
			exit;

		} elseif (array_key_exists('removeFromCart', $_GET)) {
			
			if (!GS_Session::get_city()) return;

			$model_id = filter_input(INPUT_GET, 'model_id', FILTER_VALIDATE_INT);
			$service_id = filter_input(INPUT_GET, 'service_id', FILTER_VALIDATE_INT);

			// Get Cart
			global $session_cart;
			$cart_data = $session_cart['cart_data'];

			if (count($cart_data) <= 0 || !$cart_data) {
				$cart_data = array();
			}

		

			// Get Model, CartItem collections
			 $models_query = new WP_Query(array('post_type' => 'model', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC'));
				$models = array(); 
				foreach ($models_query->posts as $modelInstance) {
				  $connected_services = new WP_Query( array(
				    'connected_type' => 'model_to_product',
				    'connected_items' => $modelInstance,
				    'nopaging' => true,
				  ) );

				  $connected_services = array_map(function($n){
				    return $n->post_title;
				  }, $connected_services->posts);

				  $connected_locations = new WP_Query( array(
				    'connected_type' => 'model_to_location',
				    'connected_items' => $modelInstance,
				    'nopaging' => true,
				  ) );

				  $connected_locations = array_map(function($n){
				    return $n->post_title;
				  }, $connected_locations->posts);

				  $thumb_id = get_post_thumbnail_id($modelInstance->ID);
				  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
				  $thumb_url = $thumb_url_array[0];

				  $modelsArray = array(
				    'id' => $modelInstance->ID,
				    'name' => $modelInstance->post_title,
				    'location' => $connected_locations,
				    'image' => $thumb_url,
				    'type' => getMetaValues(get_post_meta($modelInstance->ID, 'model-type', true)),
				    'services' => $connected_services,
				  );
				  $models[] = $modelsArray;
				}

			$services_query = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1));
			$services = array(); 
			foreach ($services_query->posts as $serviceInstance) {
			  $connected_prices = new WP_Query( array(
			    'connected_type' => 'product_to_location',
			    'connected_items' => $serviceInstance,
			    'nopaging' => true,
			  ) );



			  $locations = array_map(function($item){
			    return $item->post_title;
			  }, $connected_prices->posts);

			  $prices = array_reduce($connected_prices->posts, function($carry, $item){
			    $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'cost', true );
			    return $carry;
			  }, array());

			  $deposit = array_reduce($connected_prices->posts, function($carry, $item){
			    $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'deposit_value', true );
			    return $carry;
			  }, array());
			  
			  $servicesArray = array(
			    'id' => $serviceInstance->ID,
			    'name' => $serviceInstance->post_title,
			    'cost' => $prices,
			    'locations' => $locations,
			    'description' => wpautop($serviceInstance->post_content),
			    'pricingType' => get_post_meta($serviceInstance->ID, 'pricing-type', true),
			    'serviceCategory' => getMetaValues(get_post_meta($serviceInstance->ID, 'product-type', true)),
			    'minDuration' => (int) get_post_meta($serviceInstance->ID, 'min-duration', true),
			    'maxDuration' => (int) get_post_meta($serviceInstance->ID, 'max-duration', true),
			    'deposit' => $deposit,
			    'partyPackage' => (get_post_meta($serviceInstance->ID, 'party-package', true) == 'Yes') ? true : false
			  );
			  $services[] = $servicesArray;
			}

			// Check if service exists in cart
			$service_in_cart = array_values(array_filter($cart_data, function($cart_item) use ($service_id) {
				return $cart_item['id'] == $service_id;
			}));




			// If Service does exist in cart, add the model to the service
			if (count($service_in_cart) > 0 ) {
				// Check if model was passed

					// Update the cart
					$cart_data = array_filter(array_values($cart_data), function($cart_item) use ($service_id) {
						return $cart_item['id'] != $service_id;
					});


			} 

			// Save the cart
			$session_cart['cart_data'] = $cart_data;
			$session_cart['notifications'] = array('action' => 'remove', 'service' => $service_id);
			
			$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
			
			$new_data = set_transient( $transient_key, $session_cart, 3 * DAY_IN_SECONDS );

			$newUrl = remove_query_arg(array('removeFromCart', 'model_id', 'service_id'));

			wp_redirect($newUrl);
			
			exit;

		}
	}

	static function update_session($key, $value) {
		$session_data = $GLOBALS['session_cart'];
		$session_data[$key] = $value;
		
		$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
		
		$new_data = set_transient( $transient_key, $session_data, 3 * DAY_IN_SECONDS );
		
		//var_dump($new_data);
		return $new_data;

	}


	static function city_form() {
			$current_city = self::get_city();
			$cities = new WP_Query(array('post_type' => 'location', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC'));
			?>
			<form method="post" class="session-city" action="<?php echo site_url('/cart/update'); ?>">
				<input type="hidden" value="<?php echo $_SERVER['REQUEST_URI']; ?>" name="referrer">
				<select name="city">
					<option>Choose your City</option>
					<?php foreach($cities->posts as $city): ?>
						<option value="<?php echo $city->post_title; ?>" <?php echo ($current_city == $city->post_title) ? 'selected="selected"' : ''; ?>><?php echo $city->post_title; ?></option>
					<?php endforeach; ?>
				</select>
			</form>
		<?php }

	static function city_form_modal() { ?>
		<section class="Modal Modal__City <?php echo ( self::get_city() ) ? 'Modal__hidden' : 'Modal__visible'; ?>">
			<div class="ModalBox">

				<div class="BookOnlineWidget">
					<div class="BookOnlineWidget--title">
						<h2>Choose your Location</h2>
					</div>


					<div class="BookOnlineWidget--form">
						<?php self::city_form(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php }
	static function session_sync(){
		$inputJSON = file_get_contents('php://input');
		$input = json_decode( $inputJSON, true );
		$session_data = $GLOBALS['session_cart'];
		$session_data['cart_data'] = $input['cartItems'];
		$session_data['city'] = $input['city'];
		
		$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
		
		$new_data = set_transient( $transient_key, $session_data, 3 * DAY_IN_SECONDS );
		wp_send_json_success($new_data);
	}

}