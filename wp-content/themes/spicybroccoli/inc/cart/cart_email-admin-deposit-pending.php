<?php

$post_args = array(
    'post_type' => 'order',
    'post_status' => 'deposit-pending'
);

$post_id = wp_insert_post($post_args);

wp_insert_post(array(
    'ID' => $post_id, 
    'post_title' => 'Order #' . $post_id,
    'post_type' => 'order',
    'post_status' => 'deposit-pending'
));

global $session_cart;

update_post_meta($post_id, 'cart_data', $session_cart['cart_data']);
update_post_meta($post_id, 'city', $session_cart['city']);

$customer_data = array(
    'firstName' => $_POST['firstName'],
    'lastName' => $_POST['lastName'],
    'email' => $_POST['email'],
    'phone' => $_POST['phone']
);

update_post_meta($post_id, 'customer_data', $customer_data);

$event_details = array(
    'eventAddress' => $_POST['eventAddress'],
    'eventDateTime' => $_POST['eventDateTime'],
    'eventMessage' => $_POST['eventMessage']
);

update_post_meta($post_id, 'event_details', $event_details); 

$body = 'Hi Glamor,' . PHP_EOL;
$body .= 'You have a new  deposit order from: ' . $_POST['firstName'] . ' ' . $_POST['lastName'] . PHP_EOL;
$body .= 'Order Reference: #' . $post_id . PHP_EOL;
$body .= 'City: ' . $session_cart['city'] . PHP_EOL . PHP_EOL;

$body .= 'Customer Details' . PHP_EOL;
$body .= 'First Name: ' . $_POST['firstName'] . PHP_EOL;
$body .= 'Last Name: ' . $_POST['lastName'] . PHP_EOL;
$body .= 'Email: ' . $_POST['email'] . PHP_EOL;
$body .= 'Phone: ' . $_POST['phone'] . PHP_EOL;
$body .= PHP_EOL;

$body .= 'Event Details' . PHP_EOL;
$body .= 'Event Address: ' . $event_details['eventAddress'] . PHP_EOL;
$body .= 'Event Date/Time: ' . $event_details['eventDateTime'] . PHP_EOL;
$body .= 'Event Notes: ' . $event_details['eventNotes'] . PHP_EOL;
$body .= PHP_EOL;

$body .= 'Order Details ' . PHP_EOL;
$body .= 'Order Status: Deposit Pending' . PHP_EOL;

foreach($orders as $order):
    $body .= 'Service/Package: ' .  $order['name'] . ' | ';
    if ($order['models']) {
        $models = array_map(function($model) {
            return $model['name'];
        }, $order['models']);
            $body .= 'Wishlist: ' . implode(',', $models) . ' | ';
    }
    $body .= 'Unit Cost: ' . $order['cost'][$session_cart['city']] . ' | ';
    $body .= 'Qty: ' . $order['qty'] . ' | ';
    $body .= 'Hrs/People: ' . $order['duration'] . ' | ';
    $body .= 'Subtotal: ' . $order['subtotal'] . ' | ';
    $body .= PHP_EOL;
endforeach; 
$body .= PHP_EOL;
$body .= 'You can view this order here <' . admin_url('post.php?post='.$post_id.'&action=edit') . '>';

wp_mail( 'adrian@spicybroccoli.com', 'New Order - #'.$post_id.' [Deposit Pending]', $body ); 