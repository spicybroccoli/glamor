<table class="cart-checkout responsive">
    <thead>
        <tr>
           <th>Service</th>
           <th>Wishlist</th>
           <th>Base Cost</th>
           <th>Qty</th>
           <th>Hrs / Pax</th>
           <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($orders as $order): ?>
            <tr>
                <td><?= $order['name']; ?></td>
                <td>
                    <?php
                    if ($order['models']) {
                        $models = array_map(function($model) {
                            return $model['name'];
                        }, $order['models']);
                        echo implode(', ', $models);
                    }
                    ?>
                </td>
                <td>$<?= $order['cost'][$session_cart['city']]; ?></td>
                <td><?= $order['qty']; ?></td>
                <td><?= $order['duration']; ?></td>
                <td>$<?= $order['subtotal']; ?></td> 
            </tr>
        <?php endforeach; ?>
               </tbody>
            </table>
            <table class="cart-checkout">
            <tbody>
                <tr class="total">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>Total</strong></td>
                    <td><strong>$<?= $total; ?></strong></td>
                </tr>    
        </tbody>
       </table>