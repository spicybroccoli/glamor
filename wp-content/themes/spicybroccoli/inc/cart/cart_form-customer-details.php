<div class="formWrapper">
                <div class="cart-loader">
                    <img class="cart-loader--spinner" src="<?= site_url('/wp-content/themes/spicybroccoli/images/spinner.png'); ?>">
                </div>
             <form method="POST" class="payment-form cf" action="?step=2" data-parsley-validate>

                <div class="column">
                    <div class="form-row">
                        <label for="firstName">First Name*</label>
                        <input type="text" id="firstName" name="firstName" required />
                    </div>

                    <div class="form-row">
                        <label for="lastName">Last Name*</label>
                        <input type="text" id="lastName" name="lastName" required />
                    </div>

                    <div class="form-row">
                        <label for="email">Email*</label>
                        <input type="email" id="email" name="email" required />
                    </div>

                    <div class="form-row">
                        <label for="phone">Phone*</label>
                        <input type="text" id="phone" name="phone" required />
                    </div>

                </div>

                <div class="column">

                    <div class="form-row">
                        <label for="eventAddress">Event Address*</label>
                        <input type="text" id="eventAddress" name="eventAddress" required />
                    </div>

                    <div class="form-row">
                        <label for="eventDateTime">Event Date/Time*</label>
                        <input type="text" name="eventDateTime" required data-format="dd-MMM-yyyy hh:mm:ss AA" data-field="datetime" readonly />
                        <div id="eventDateTime"></div>
                    </div>

                    <div class="form-row">
                        <label for="eventMessage">Message</label>
                        <textarea id="eventMessage" name="eventMessage" rows="5"></textarea>
                    </div>

                </div>

                <div class="form-row form-row-submit">
                    <input type="submit" value="Next" text="Next" />
                </div>

             </form>
             </div>