<?php

$post_args = array(
    'post_type' => 'order',
    'post_status' => 'enquiry'
);

$post_id = wp_insert_post($post_args);

wp_insert_post(array('ID' => $post_id, 'post_title' => 'Order #' . $post_id, 'post_status' => 'enquiry', 'post_type' => 'order'));

global $session_cart;

update_post_meta($post_id, 'cart_data', $session_cart['cart_data']);
update_post_meta($post_id, 'city', $session_cart['city']);

$customer_data = array(
    'firstName' => $_POST['firstName'],
    'lastName' => $_POST['lastName'],
    'email' => $_POST['email'],
    'phone' => $_POST['phone']
);

update_post_meta($post_id, 'customer_data', $customer_data);

$event_details = array(
    'eventAddress' => $_POST['eventAddress'],
    'eventDateTime' => $_POST['eventDateTime'],
    'eventMessage' => $_POST['eventMessage']
);

update_post_meta($post_id, 'event_details', $event_details); 


// Admin Email

$body = 'Hi Glamor,' . PHP_EOL;
$body .= 'You have a new enquiry from: ' . $_POST['firstName'] . ' ' . $_POST['lastName'] . PHP_EOL;
$body .= 'Order Reference: #' . $post_id . PHP_EOL;
$body .= 'City: ' . $session_cart['city'] . PHP_EOL . PHP_EOL;

$body .= 'Customer Details' . PHP_EOL;
$body .= 'First Name: ' . $_POST['firstName'] . PHP_EOL;
$body .= 'Last Name: ' . $_POST['lastName'] . PHP_EOL;
$body .= 'Email: ' . $_POST['email'] . PHP_EOL;
$body .= 'Phone: ' . $_POST['phone'] . PHP_EOL;
$body .= PHP_EOL;

$body .= 'Event Details' . PHP_EOL;
$body .= 'Event Address: ' . $event_details['eventAddress'] . PHP_EOL;
$body .= 'Event Date/Time: ' . $event_details['eventDateTime'] . PHP_EOL;
$body .= 'Event Notes: ' . $event_details['eventNotes'] . PHP_EOL;
$body .= PHP_EOL;

$body .= 'Order Details ' . PHP_EOL;
$body .= 'Order Status: Total Pending' . PHP_EOL;

foreach($orders as $order):
    $body .= 'Service/Package: ' .  $order['name'] . ' | ';
    if ($order['models']) {
        $models = array_map(function($model) {
            return $model['name'];
        }, $order['models']);
            $body .= 'Wishlist: ' . implode(',', $models) . ' | ';
    }
    $body .= 'Unit Cost: ' . $order['cost'][$session_cart['city']] . ' | ';
    $body .= 'Qty: ' . $order['qty'] . ' | ';
    $body .= 'Hrs/People: ' . $order['duration'] . ' | ';
    $body .= 'Subtotal: ' . $order['subtotal'];
    $body .= PHP_EOL;
endforeach; 
$body .= PHP_EOL;
$body .= 'You can view this enquiry here <' . admin_url('post.php?post='.$post_id.'&action=edit') . '>';

wp_mail( 'adrian@spicybroccoli.com', 'New Enquiry', $body ); 


// End Admin Email


// Customer Email
$customer_details = get_post_meta($post_id, 'customer_data', true);
$event_details = get_post_meta($post_id, 'event_details', true);
$orders = get_post_meta($post_id, 'cart_data', true);
$city = get_post_meta($post_id, 'city', true);

$body = 'Hi ' . $customer_details['firstName'] . ',' . PHP_EOL;
$body .= 'Your enquiry has been received - we\'ll be in touch shortly!' . PHP_EOL . PHP_EOL;
$body .= 'Enquiry Reference: #' . $post_id . PHP_EOL;
$body .= 'City: ' . $city . PHP_EOL . PHP_EOL;

$body .= 'Customer Details' . PHP_EOL;
$body .= 'First Name: ' . $customer_details['firstName'] . PHP_EOL;
$body .= 'Last Name: ' . $customer_details['lastName'] . PHP_EOL;
$body .= 'Email: ' . $customer_details['email'] . PHP_EOL;
$body .= 'Phone: ' . $customer_details['phone'] . PHP_EOL;
$body .= PHP_EOL;

$body .= 'Event Details' . PHP_EOL;
$body .= 'Event Address: ' . $event_details['eventAddress'] . PHP_EOL;
$body .= 'Event Date/Time: ' . $event_details['eventDateTime'] . PHP_EOL;
$body .= 'Event Notes: ' . $event_details['eventNotes'] . PHP_EOL;
$body .= PHP_EOL;


$body .= 'Enquiry Details ' . PHP_EOL;
foreach($orders as $order):
    $body .= 'Service/Package: ' .  $order['name'] . ' | ';
    if ($order['models']) {
        $models = array_map(function($model) {
            return $model['name'];
        }, $order['models']);
            $body .= 'Wishlist: ' . implode(', ', $models) . ' | ';
    }
    $body .= 'Unit Cost: ' . $order['cost'][$city] . ' | ';
    $body .= 'Qty: ' . $order['qty'] . ' | ';
    $body .= 'Hrs/People: ' . $order['duration'] . ' | ';
    $body .= 'Subtotal: ' . $order['subtotal'];
    $body .= PHP_EOL;
endforeach; 
$body .= PHP_EOL;

wp_mail( $customer_details['email'],  $_POST['firstName'] . ', thanks for your enquiry', $body ); 
// End Customer Email

// Lets clear the cart
// Keep the location saved though
global $session_cart;
$session_cart['cart_data'] = array();
$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
$new_data = set_transient( $transient_key, $session_data, 3 * DAY_IN_SECONDS );