<?php
add_action( 'init', 'register_taxonomy_product_categories' );

function register_taxonomy_product_categories() {

    $labels = array( 
        'name' => _x( 'Product Categories', 'product_categories' ),
        'singular_name' => _x( 'Product Category', 'product_categories' ),
        'search_items' => _x( 'Search Product Categories', 'product_categories' ),
        'popular_items' => _x( 'Popular Product Categories', 'product_categories' ),
        'all_items' => _x( 'All Product Categories', 'product_categories' ),
        'parent_item' => _x( 'Parent Product Category', 'product_categories' ),
        'parent_item_colon' => _x( 'Parent Product Category:', 'product_categories' ),
        'edit_item' => _x( 'Edit Product Category', 'product_categories' ),
        'update_item' => _x( 'Update Product Category', 'product_categories' ),
        'add_new_item' => _x( 'Add New Product Category', 'product_categories' ),
        'new_item_name' => _x( 'New Product Category', 'product_categories' ),
        'separate_items_with_commas' => _x( 'Separate product categories with commas', 'product_categories' ),
        'add_or_remove_items' => _x( 'Add or remove Product Categories', 'product_categories' ),
        'choose_from_most_used' => _x( 'Choose from most used Product Categories', 'product_categories' ),
        'menu_name' => _x( 'Product Categories', 'product_categories' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => false,

        'rewrite' => array( 
            'slug' => 'service-category', 
            'with_front' => false,
            'hierarchical' => true
        ),
        'query_var' => true
    );

    register_taxonomy( 'product_categories', array('product'), $args );
}