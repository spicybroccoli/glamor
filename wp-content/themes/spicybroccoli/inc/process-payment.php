<?php

define('COOKIE_KEY', 'gs_session_cookie'); 
define('TRANSIENT_KEY', 'gs_'); // Need to keep transient key under 45 chars

 require('lib/eWAY/RapidAPI.php');

 $request = new eWAY\GetAccessCodeResultRequest();
 $request->AccessCode = $_GET['AccessCode'];

 $service = new eWAY\RapidAPI(
     '60CF3C6jTZkBVIfK5heEc8suXQssuyOddprG5Gn8qI0yLuzwVPMnojvhntU7RHnKpc7Qt6', 
     'D1nnert1me',  
     array('sandbox' => true)
 );

 $result = $service->GetAccessCodeResult($request);

 if ($result->TransactionStatus) {
 	// Payment was successful

 	// Update post status
 	$order = get_post($result->InvoiceReference);

  $customer_details = get_post_meta($order->ID, 'customer_data', true);
  $event_details = get_post_meta($order->ID, 'event_details', true);
  $orders = get_post_meta($order->ID, 'cart_data', true);
  $city = get_post_meta($order->ID, 'city', true);
  $order_subject = 'Order Confirmation - #' . $order->ID;



  // Dont send email if post status already updated!
  if ( $order->post_status == 'deposit-pending' || $order->post_status == 'total-pending' ) {
      var_dump('I was called');

 	$post_status = '';

 	if ($order->post_status == 'deposit-pending') {
 		$post_status = 'deposit-paid';
 		$email_label = 'Deposit Paid';
 	} else if ($order->post_status == 'total-pending') {
 		$post_status = 'total-paid';
 		$email_label = 'Total Paid';
 	}

 	wp_insert_post(array(
 			'ID' => $order->ID,
 			'post_title' => $order->post_title,
 			'post_status' => $post_status,
 			'post_type' => 'order'
 		)
 	);

 	// Send confirmation email to admin and customer

 	// Admin email;
 	$body = 'Hi Glamor,' . PHP_EOL;
 	$body .= 'Order #' . $order->ID . ' has been successfully processed.' . PHP_EOL . PHP_EOL;
 	$body .= 'Order Details ' . PHP_EOL;
 	$body .= 'Order Reference: #' . $order->ID . PHP_EOL;
 	$body .= 'Order Status: ' . $email_label . PHP_EOL;
 	
 	$body .= PHP_EOL;
 	$body .= 'You can view this order here <' . admin_url('post.php?post='.$order->ID.'&action=edit') . '>';

 	wp_mail( 'adrian@spicybroccoli.com', 'Order Processed - #'.$order->ID.' ['.$email_label.']', $body );


 	// Customer Email


 	$body = 'Hi ' . $customer_details['firstName'] . ', your order has been successfully processed' . PHP_EOL . PHP_EOL;
 	$body .= 'Order Reference: #' . $order->ID . PHP_EOL;
 	$body .= 'City: ' . $city . PHP_EOL . PHP_EOL;

 	$body .= 'Customer Details' . PHP_EOL;
 	$body .= 'First Name: ' . $customer_details['firstName'] . PHP_EOL;
 	$body .= 'Last Name: ' . $customer_details['lastName'] . PHP_EOL;
 	$body .= 'Email: ' . $customer_details['email'] . PHP_EOL;
 	$body .= 'Phone: ' . $customer_details['phone'] . PHP_EOL;
 	$body .= PHP_EOL;

 	$body .= 'Event Details' . PHP_EOL;
 	$body .= 'Event Address: ' . $event_details['eventAddress'] . PHP_EOL;
 	$body .= 'Event Date/Time: ' . $event_details['eventDateTime'] . PHP_EOL;
 	$body .= 'Event Notes: ' . $event_details['eventNotes'] . PHP_EOL;
 	$body .= PHP_EOL;


 	$body .= 'Order Details ' . PHP_EOL;
 	foreach($orders as $order):
 	    $body .= 'Service/Package: ' .  $order['name'] . ' | ';
 	    if ($order['models']) {
 	        $models = array_map(function($model) {
 	            return $model['name'];
 	        }, $order['models']);
 	            $body .= 'Models: ' . implode(', ', $models) . ' | ';
 	    }
 	    $body .= 'Unit Cost: ' . $order['cost'][$city] . ' | ';
 	    $body .= 'Qty: ' . $order['qty'] . ' | ';
 	    $body .= 'Hrs/People: ' . $order['duration'] . ' | ';
 	    $body .= 'Subtotal: ' . $order['subtotal'];
 	    $body .= PHP_EOL;
 	endforeach; 
 	$body .= PHP_EOL;

 	

 	wp_mail( $customer_details['email'], $order_subject, $body ); 


   	// Lets clear the cart
   	// Keep the location saved though
   	global $session_cart;
   	$session_cart['cart_data'] = array();
   	$transient_key = TRANSIENT_KEY . $_COOKIE[COOKIE_KEY];
   	$new_data = set_transient( $transient_key, $session_cart, 3 * DAY_IN_SECONDS );
  }
 } else {
 	$errors = explode(',', $result->ResponseMessage);
 }

 ?>

<?php get_header(); ?>

<section class="panel panelCheckout">
  <div class="wrapper cf">
  <?php if ($result->TransactionStatus): ?>
  	 <?php $order = get_post($result->InvoiceReference); ?>
    <div class="payment-success">
    <h2>Checkout</h2>
    <h3>Step 3 - Payment Confirmed</h3>

    <p>Your payment was successful. Your order reference number is <strong>#<?= $order->ID; ?></strong>. 
	   <br/>A summary of your order has been emailed to <strong><?= $customer_details['email']; ?></strong>
     <br/>Please find the details of your order below.</p>
    <?php
      $total = array_reduce($orders, function($carry, $item){
            $subtotal = (int) $item['subtotal'];
            $carry += $subtotal;
            return $carry;
        }, 0);

          $deposit_total = array_reduce($orders, function($carry, $item) use ($city) {
                $subtotal = (int) $item['deposit'][$city];
                $carry += $subtotal;
                return $carry;
            }, 0);
        ?>

    <h4>Your Order</h4>
    <table class="cart-checkout">
        <thead>
            <tr>
               <th>Service</th>
               <th>Models</th>
               <th>Base Cost</th>
               <th>Qty</th>
               <th>Hrs</th>
               <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($orders as $order): ?>
                <tr>
                    <td><?= $order['name']; ?></td>
                    <td>
                        <?php
                        if ($order['models']) {
                            $models = array_map(function($model) {
                                return $model['name'];
                            }, $order['models']);
                            echo implode(', ', $models);
                        }
                        ?>
                    </td>
                    <td>$<?= $order['cost'][$city]; ?></td>
                    <td><?= $order['qty']; ?></td>
                    <td><?= $order['duration']; ?></td>
                    <td>$<?= $order['subtotal']; ?></td> 
                </tr>
            <?php endforeach; ?>
                <tr class="total">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>Deposit</strong></td>
                    <td><strong>$<?= $deposit_total; ?></strong></td>
                </tr>  
                <tr class="total">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><strong>Total</strong></td>
                    <td><strong>$<?= $total; ?></strong></td>
                </tr>    
        </tbody>
       </table>

       <h4>Your Contact Details</h4>
       <table class="cart-checkout cart-contact">
            <tr>
                <td>First Name:</td>
                <td><?php echo $customer_details['firstName']; ?></td>
            </tr>
            <tr>
                <td>Last Name:</td>
                <td><?php echo $customer_details['lastName']; ?></td>
            </tr>
            <tr>
                <td>Email:</td>
                <td><?php echo $customer_details['email']; ?></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td><?php echo $customer_details['phone']; ?></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><?php echo $city; ?></td>
            </tr>
            <tr>
                <td>Event Address:</td>
                <td><?php echo $event_details['eventAddress']; ?></td>
            </tr>
            <tr>
                <td>Event Date/Time:</td>
                <td><?php echo $event_details['eventDateTime']; ?></td>
            </tr>
            <tr>
                <td>Message:</td>
                <td><?php echo $event_details['eventMessage']; ?></td>
            </tr>
        </table>


       </div>

   <?php else: ?>
    <div class="payment-error">
   	<h2>Error processing payment</h2>
	<?php foreach($errors as $error): ?>
   	<p><?= eWay\ResponseCode::getMessage($error); ?></p>
   <?php endforeach; ?>
   </div>
   <?php endif; ?>
  </div>
</section>

<?php get_footer(); ?>
 