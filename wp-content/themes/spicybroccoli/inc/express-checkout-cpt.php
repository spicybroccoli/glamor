<?php
add_action( 'init', 'register_cpt_express_orders' );

function register_cpt_express_orders() {

    $labels = array( 
        'name' => _x( 'Express Orders', 'express_orders' ),
        'singular_name' => _x( 'Express Order', 'express_orders' ),
        'add_new' => _x( 'Add New', 'express_orders' ),
        'add_new_item' => _x( 'Add New Express Order', 'express_orders' ),
        'edit_item' => _x( 'Edit Express Order', 'express_orders' ),
        'new_item' => _x( 'New Express Order', 'express_orders' ),
        'view_item' => _x( 'View Express Order', 'express_orders' ),
        'search_items' => _x( 'Search Express Orders', 'express_orders' ),
        'not_found' => _x( 'No express orders found', 'express_orders' ),
        'not_found_in_trash' => _x( 'No express orders found in Trash', 'express_orders' ),
        'parent_item_colon' => _x( 'Parent Express Order:', 'express_orders' ),
        'menu_name' => _x( 'Express Orders', 'express_orders' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title' ),
        
        'public' => true,
        'show_ui' => true,
        
        
        'menu_icon' => 'dashicons-randomize',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => true,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('with_front' => false, 'slug' => 'express-order'),
        'capability_type' => 'post'
    );

    register_post_type( 'express_orders', $args );
}

function express_sync(){

   

       $inputJSON = file_get_contents('php://input');
       $input = json_decode( $inputJSON, true );

       $pid = $_GET['id'];

      update_post_meta($pid, 'city', $input['city']);
        update_post_meta($pid, 'cart_data', $input['cartItems']);
      
       wp_send_json_success($post->ID);

        
 
}

add_action('wp_ajax_express_sync', 'express_sync');


add_action('admin_init', function() {
    add_meta_box('expressorderdetailsdiv', 'Order Details', function($post) { ?>
          
          <div class="error">
          <p><strong>This express order will auto-save</strong></p>
          </div>
           <div class="PageContent cf">
           
             <div class="PageContent--BookingProcess">
               <div class="BookingForm">
                 <div id="checkout"></div>
                 <div id="checkout-actions"></div>
               </div>
             </div>
           
           <div class="PageContent--sidebar">
             <div class="PageContent--sidebar-cart">
             <div class="CartForm">
               <div class="PageContent--sidebar-cart-headline cf">
                 <div class="PageContent--sidebar-cart-headline--text">Express Order</div>
           
               </div>
               <div id="cartContents"></div>
               
             </div>
             <div id="cart-actions"></div>
             </div><!-- .PageContent--sidebar-cart -->
           </div><!-- .PageContent--sidebar -->
            </div>
             </div>

              <?php
                    $models_query = new WP_Query(array('post_type' => 'model', 'posts_per_page' => -1));
                    $models = array(); 
                    foreach ($models_query->posts as $modelInstance) {
                      $connected_services = new WP_Query( array(
                        'connected_type' => 'model_to_product',
                        'connected_items' => $modelInstance,
                        'nopaging' => true,
                      ) );

                      $connected_services = array_map(function($n){
                        return $n->post_title;
                      }, $connected_services->posts);

                      $connected_locations = new WP_Query( array(
                        'connected_type' => 'model_to_location',
                        'connected_items' => $modelInstance,
                        'nopaging' => true,
                      ) );

                      $connected_locations = array_map(function($n){
                        return $n->post_title;
                      }, $connected_locations->posts);

                      $thumb_id = get_post_thumbnail_id($modelInstance->ID);
                      $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
                      $thumb_url = $thumb_url_array[0];

                      $modelsArray = array(
                        'id' => $modelInstance->ID,
                        'name' => $modelInstance->post_title,
                        'location' => $connected_locations,
                        'image' => $thumb_url,
                        'type' => getMetaValues(get_post_meta($modelInstance->ID, 'model-type', true)),
                        'services' => $connected_services,
                      );
                      $models[] = $modelsArray;
                    }
                    echo '<script>window.models =' . json_encode($models) . '; </script>';
                    
                    // {id: 4, name: 'Service 4', cost: 401, location: ['Wollongong', 'Sydney'], service_category: ['Cat 1', 'Cat 2']}
                    $services_query = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1));
                    $services = array(); 
                    foreach ($services_query->posts as $serviceInstance) {
                      $connected_prices = new WP_Query( array(
                        'connected_type' => 'product_to_location',
                        'connected_items' => $serviceInstance,
                        'nopaging' => true,
                      ) );

                      $locations = array_map(function($item){
                        return $item->post_title;
                      }, $connected_prices->posts);

                      $prices = array_reduce($connected_prices->posts, function($carry, $item){
                        $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'cost', true );
                        return $carry;
                      }, array());

                      $deposit = array_reduce($connected_prices->posts, function($carry, $item){
                        $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'deposit_value', true );
                        return $carry;
                      }, array());
                      
                      $servicesArray = array(
                        'id' => $serviceInstance->ID,
                        'name' => $serviceInstance->post_title,
                        'cost' => $prices,
                        'locations' => $locations,
                        'description' => wpautop($serviceInstance->post_content),
                        'pricingType' => get_post_meta($serviceInstance->ID, 'pricing-type', true),
                        'serviceCategory' => getMetaValues(get_post_meta($serviceInstance->ID, 'product-type', true)),
                        'minDuration' => (int) get_post_meta($serviceInstance->ID, 'min-duration', true),
                        'maxDuration' => (int) get_post_meta($serviceInstance->ID, 'max-duration', true),
                        'deposit' => $deposit,
                        'partyPackage' => (get_post_meta($serviceInstance->ID, 'party-package', true) == 'Yes') ? true : false
                      );
                      $services[] = $servicesArray;
                    }

                    echo '<script>window.services =' . json_encode($services) . '; </script>';

                    $cities = new WP_Query(array('post_type' => 'location', 'posts_per_page' => -1));

                    $citiesValues = array_map(function($item){
                        return array('id' => $item->ID, 'name' => $item->post_title);
                    }, $cities->posts);

                    echo '<script>window.cities =' . json_encode($citiesValues) . '; </script>';
                    
                    if (get_post_meta($post->ID, 'city', true)) {
                        echo '<script> window.currentCity = "' . get_post_meta($post->ID, 'city', true) . '"; </script>';
                    }
                    

                    if (get_post_meta($post->ID, 'cart_data', true)) {
                        echo '<script>window.initialCart =' . json_encode(array_unique(get_post_meta($post->ID, 'cart_data', true), SORT_REGULAR)) . '; </script>';
                    } else {
                        echo '<script>window.initialCart = [];</script>';
                    }

                    $ajaxurl = '/glamor/wp-admin/admin-ajax.php?action=express_sync&id=' . $post->ID;
                    
              ?>
            <script>window.GS_ajaxurl = '<?php echo $ajaxurl; ?>'; </script>
            <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/cart/bundle.js"></script>
      
      <?php }, 'express_orders', 'normal');
});