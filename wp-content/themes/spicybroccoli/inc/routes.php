<?php

add_filter( 'init', function()
{
    global $wp;
    $wp->add_query_var( 'model_gender' );
    $wp->add_query_var( 'party_type' );
    $wp->add_query_var( 'party_packages' );
    $wp->add_query_var( 'blog' );
    $wp->add_query_var( 'cart' );
    $wp->add_query_var( 'model_archive' );

});

  add_action( 'init', 'sbm_rewrite', 9 );

  function sbm_rewrite() {


      add_rewrite_rule('^strippers(?:/(male|female))?$', 'index.php?post_type=model&model_gender=$matches[1]&model_archive=stripper', 'top');
      add_rewrite_rule('^models(?:/(male|female))?$', 'index.php?post_type=model&model_gender=$matches[1]&model_archive=model', 'top');

      add_rewrite_rule('^party-packages(?:/(male|female))?$', 'index.php?post_type=product&party_type=$matches[1]&party_packages=archive', 'top');
      add_rewrite_rule('^party-packages/([a-zA-z\-]+)?$', 'index.php?post_type=product&product=$matches[1]&party_packages=single', 'top');
      

      // Blog Rewrite Rules
      add_rewrite_rule('^blog$', 'index.php?post_type=post&blog=archive', 'top');
      add_rewrite_rule('^blog(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&blog=archive&paged=$matches[1]', 'top');
      add_rewrite_rule('^blog\/tag\/([a-zA-z\-]+)(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&tag=$matches[1]&paged=$matches[2]&blog=archive', 'top');
      add_rewrite_rule('^blog\/category\/([a-zA-z\-]+)(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&tcategory_name=$matches[1]&paged=$matches[2]&blog=archive', 'top');
      add_rewrite_rule('^blog\/([0-9]{4})(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&year=$matches[1]&paged=$matches[2]&blog=archive', 'top');
      add_rewrite_rule('^blog\/([0-9]{4})\/([0-9]{1,2})(?:\/page\/([0-9]+))?$', 'index.php?post_type=post&year=$matches[1]&month=$matches[2]&paged=$matches[3]&blog=archive', 'top');

      // Cart Pages
      add_rewrite_rule('^cart$', 'index.php?cart=cart', 'top');
      add_rewrite_rule('^book-online$', 'index.php?cart=book-online', 'top');
      add_rewrite_rule('^cart/update$', 'index.php?cart=update', 'top');
      add_rewrite_rule('^cart/checkout$', 'index.php?cart=checkout', 'top');
      add_rewrite_rule('^cart/checkout/deposit$', 'index.php?cart=pay-deposit', 'top');
      add_rewrite_rule('^cart/checkout/enquiry$', 'index.php?cart=enquiry', 'top');
      add_rewrite_rule('^cart/checkout/process$', 'index.php?cart=process', 'top');


  }


// Parse Request for our custom rewrites.
add_action( 'parse_query', function($query){

  // Don't run this if in admin. Bail. Immediately. Get out. go. hurry.
  if (is_admin()) {
    return;
  }


  if (array_key_exists('cart', $query->query_vars)) {
    $query->is_home = false;
    $query->is_404 = false;

    if ($query->query_vars['cart'] == 'cart') {
      add_filter('body_class', function($classes){
        $classes[] = 'page-cart';
        return $classes;
      }, 10, 1);
    }

    add_filter( 'wp_title', function($title, $sep ) {
      global $paged, $page;
      global $wp_query;


      // Add the site name.
      $title .= get_bloginfo( 'name' );

      // Grab the query var and capitalise it and replace dashes with spaces
      $page = ucwords(str_replace('-', ' ', $wp_query->query_vars['cart']));

      $title = "$page $sep ";


      return $title;
    }, 10, 2);
    
  }

  if (!empty($query->query_vars['s'])) {
    
    if (GS_Session::get_city()) {
      $id =  get_page_by_path( GS_Session::get_city(), OBJECT, 'location');
      $query->set('connected_type', array('product_to_location', 'model_to_location'));
      $query->set('connected_items', $id->ID);
      $query->set('connected_direction', 'to');
    
    }
  }

  if ($query->query_vars['blog'] == 'archive') {
    $query->is_home = false;
    $query->is_404 = false;
    $query->is_archive = true;
  }

  if ($query->query_vars['party_packages'] == 'archive') {

    if (GS_Session::get_city()) {
      $id =  get_page_by_path( GS_Session::get_city(), $output, 'location');
      $query->set('connected_type', 'product_to_location');
      $query->set('connected_items', $id->ID);
      $query->set('connected_direction', 'to');
    }

    $query->is_home = false;
    $query->is_404 = false;

      $meta_query = array(
        array(
          'key'     => 'party-package',
          'value'   => 'Yes',
          'compare' => 'LIKE',
        ));

       $query->query_vars['meta_query'] = $meta_query;




  } else if ($query->query_vars['post_type'] == 'model' && $query->query_vars['model_archive'] == 'stripper') {

    if ($query->query_vars['model_gender']) {
      if (GS_Session::get_city()) {
        $id =  get_page_by_path( GS_Session::get_city(), $output, 'location');
        $query->set('connected_type', 'model_to_location');
        $query->set('connected_items', $id->ID);
        $query->set('connected_direction', 'to');
      }

      $query->is_home = false;
      $wp_query->is_404 = false;

      $meta_query = array(
        array(
          'key'     => '_model_type',
          'value'   => serialize(strval('stripper')),
          'compare' => 'LIKE',
        ));


        
          $meta_query['relation'] = 'AND';
          $meta_query[] = array(
            'key' => '_model_gender',
            'value' => ucfirst(strval($query->query_vars['model_gender'])),
            'compare' => '='
          );

          // Add title filter 
          add_filter( 'wp_title', function($title, $sep ) {
            global $wp_query;


            // Add the site name.
            $title .= get_bloginfo( 'name' );

            // Grab the query var and capitalise it and replace dashes with spaces
            $page = ucwords($wp_query->query_vars['model_gender']);

            $title = "$page Strippers $sep ";


            return $title;
          }, 10, 2);
        

      $query->query_vars['meta_query'] = $meta_query;
  } else {
    wp_redirect(site_url('/strippers/female'), 301);
    exit;
  }



    
  
  } else if ($query->query_vars['post_type'] == 'model' && $query->query_vars['model_archive'] == 'model' ) {

    if ($query->query_vars['model_gender']) {
      if (GS_Session::get_city()) {
        $id =  get_page_by_path( GS_Session::get_city(), $output, 'location');
        $query->set('connected_type', 'model_to_location');
        $query->set('connected_items', $id->ID);
        $query->set('connected_direction', 'to');
      }

      $wp_query->is_home = false;
      $wp_query->is_404 = false;

      $meta_query = array(
        array(
          'key'     => '_model_type',
          'value'   => serialize(strval('model')),
          'compare' => 'LIKE',
        ));


        
          $meta_query['relation'] = 'AND';
          $meta_query[] = array(
            'key' => '_model_gender',
            'value' => ucfirst(strval($query->query_vars['model_gender'])),
            'compare' => '='
          );
          add_filter( 'wp_title', function($title, $sep ) {
            global $wp_query;


            // Add the site name.
            $title .= get_bloginfo( 'name' );

            // Grab the query var and capitalise it and replace dashes with spaces
            $page = ucwords($wp_query->query_vars['model_gender']);

            $title = "$page Models $sep ";


            return $title;
          }, 10, 2);
        

      $query->query_vars['meta_query'] = $meta_query;
  } else {
    wp_redirect(site_url('/models/female'), 301);
    exit;
  }

    
  
  } 

  


}, 1);

add_filter('template_include', function($template) {
  global $wp_query;

  if ($wp_query->query_vars['party_packages'] == 'archive') {
    $template = locate_template('archive-party-packages.php');

  } elseif ($wp_query->query_vars['party_packages'] == 'single') {
    $template = locate_template('single-product.php');

  } elseif ($wp_query->query_vars['cart']) {

    if ( $wp_query->query_vars['cart'] == 'cart') {
      $template = locate_template('cart.php');

    } elseif ( $wp_query->query_vars['cart'] == 'book-online') {
      $template = locate_template('cart-book-online.php');

    } elseif ( $wp_query->query_vars['cart'] == 'update') {
      $template = locate_template('cart-update.php');

    } elseif ( $wp_query->query_vars['cart'] == 'checkout' || $wp_query->query_vars['cart'] == 'pay-deposit' || $wp_query->query_vars['cart'] == 'enquiry') {
      $template = locate_template('cart-checkout.php');

    } elseif ( $wp_query->query_vars['cart'] == 'process') {
      $template = locate_template('inc/process-payment.php');
    }


  }
  return $template;
}, 10, 1);

// Party Package URL Links

function append_query_string( $url, $post ) {
    if ( 'product' == get_post_type( $post ) ) {
      if (get_post_meta($post->ID, 'party-package', true) == 'Yes' ) {
        return site_url('/party-packages/'.$post->post_name);
      } else {
        return site_url('/services');
      }
    }
    return $url;
}
add_filter( 'post_type_link', 'append_query_string', 10, 2 );
