<?php
add_action( 'init', function() {

    $labels = array( 
        'name' => _x( 'Orders', 'order' ),
        'singular_name' => _x( 'Order', 'order' ),
        'add_new' => _x( 'Add New', 'order' ),
        'add_new_item' => _x( 'Add New Order', 'order' ),
        'edit_item' => _x( 'Edit Order', 'order' ),
        'new_item' => _x( 'New Order', 'order' ),
        'view_item' => _x( 'View Orders', 'order' ),
        'search_items' => _x( 'Search Orders', 'order' ),
        'not_found' => _x( 'No Orders found', 'order' ),
        'not_found_in_trash' => _x( 'No Order found in Trash', 'order' ),
        'parent_item_colon' => _x( 'Parent Order:', 'order' ),
        'menu_name' => _x( 'Orders', 'order' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title' ),
        
        'public' => true,
        'show_ui' => true,
        
        'menu_icon' => 'dashicons-tickets-alt',
        'rewrite'            => false,
        'query_var'          => false,
        'publicly_queryable' => false,
        'show_in_nav_menus' => true,
        'exclude_from_search' => true,
        'has_archive' => true,
        'can_export' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'order', $args );
});

function remove_quick_edit( $actions ) {
    global $post;
    if( $post->post_type == 'order' ) {
        $actions['edit'] = '<a href="' . admin_url("post.php?post=" . $post->ID . "&action=edit") . '" title="View Order #'.$post->ID.' rel="permalink">View</a>';
        unset($actions['inline hide-if-no-js']);
        unset($actions['view']);
        
    }
    return $actions;
}

if (is_admin()) {
    add_filter('post_row_actions','remove_quick_edit',10,2);
}


function my_custom_post_status(){
    $post_statuses = array(
        array('key' => 'enquiry', 'label' => 'Enquiry'),
        array('key' => 'deposit-pending', 'label' => 'Deposit Pending'),
        array('key' => 'deposit-paid', 'label' => 'Deposit Paid'),
        array('key' => 'total-pending', 'label' => 'Total Pending'),
        array('key' => 'total-paid', 'label' => 'Total Paid')
    );
    foreach($post_statuses as $post_status) {
        register_post_status( $post_status['key'], array(
            'label'                     => $post_status['label'],
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop(  $post_status['label']. ' <span class="count">(%s)</span>', $post_status['label'] . ' <span class="count">(%s)</span>' ),
        ) );
    }
}
add_action( 'init', 'my_custom_post_status' );

add_action( 'post_submitbox_misc_actions', 'my_post_submitbox_misc_actions' );
function my_post_submitbox_misc_actions(){

?>


<?php

global $post;

//only when editing a post
if( $post->post_type == 'order' ){

    global $wp_post_statuses;

    // custom post status: approved
    $complete = '';
    $label = ''; 

    $statuses = array_filter($wp_post_statuses, function($status){
        return ( $status->_builtin == false );
    });

    foreach($statuses as $status) {
      if( $post->post_status == $status->name){
          $complete = ' selected=\"selected\"';
          $label = $status->label;
      }  
    }

    ?>
    <script>
    jQuery(document).ready(function($){
        var items = <?= json_encode(array_values($statuses)); ?>;
        console.log([].slice.call(items));
        var currentlySelected = '<?= $post->post_status; ?>'
        console.log(currentlySelected);
        var html = items.map(function(item){
            var selected = (item.name == currentlySelected) ? " selected='selected'" : "";
            console.log(selected);
            return "<option value='" + item.name + "' "+ selected +">" + item.label + "</option>"
        })
        
        $("select#post_status").append(html.join(''));
        $("#post-status-display").text("<?= $label; ?>");
        $('#title').attr('disabled', 'disabled')
    });
    </script>

<?php }
}


add_action('admin_init', function() {
    add_meta_box('Order Details', 'Order Details', function($post) {
      
        $orders = get_post_meta($post->ID, 'cart_data', true);



        $total = array_reduce($orders, function($carry, $item) {
            $subtotal = (int) $item['subtotal'];
            $carry += $subtotal;
            return $carry;
        }, 0);

        $depositTotal = array_reduce($orders, function($carry, $item) use ($post) {
            $city = get_post_meta($post->ID, 'city', true);
            $subtotal = (int) $item['deposit'][$city];
            $carry += $subtotal;
            return $carry;
        }, 0);

        ?>

        <h3>City</h3>
        <?php echo get_post_meta($post->ID, 'city', true); ?>

        <h3>Cart Items</h3>
        <table>
         <thead>
             <tr>
                <th>Service</th>
                <th>Models</th>
                <th>Base Cost</th>
                <th>Qty</th>
                <th>Hrs</th>
                <th>Subtotal</th>
             </tr>
         </thead>
         <tbody>
             <?php foreach($orders as $order): ?>
                 <tr>
                     <td><?= $order['name']; ?></td>
                     <td>
                         <?php
                         if ($order['models']) {
                             $models = array_map(function($model) {
                                 return $model['name'];
                             }, $order['models']);
                             echo implode(',', $models);
                         }
                         ?>
                     </td>
                     <td><?= $order['cost'][$session_cart['city']]; ?></td>
                     <td><?= $order['qty']; ?></td>
                     <td><?= $order['duration']; ?></td>
                     <td>$<?= $order['subtotal']; ?></td> 
                 </tr>
             <?php endforeach; ?>
             <tr>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                 </tr>
                 <tr>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td><strong>Deposit Total</strong></td>
                     <td><strong>$<?= $depositTotal; ?></strong></td>
                 </tr>  
                 <tr>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td><strong>Total</strong></td>
                     <td><strong>$<?= $total; ?></strong></td>
                 </tr>    
         </tbody>
        </table>

        <h3>Customer Details</h3>
        <?php $customer_details = get_post_meta($post->ID, 'customer_data', true); ?>
        <table>
            <tr>
                <td>First Name</td>
                <td><?= $customer_details['firstName']; ?></td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td><?= $customer_details['lastName']; ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?= $customer_details['email']; ?></td>
            </tr>
            <tr>
                <td>Phone</td>
                <td><?= $customer_details['phone']; ?></td>
            </tr>
        </table>

        <h3>Event Details</h3>
        <?php $event_details = get_post_meta($post->ID, 'event_details', true); ?>
        <table>
            <tr>
                <td>Event Address</td>
                <td><?= $event_details['eventAddress']; ?></td>
            </tr>
            <tr>
                <td>Event Date/Time</td>
                <td><?= $event_details['eventDateTime']; ?></td>
            </tr>
            <tr>
                <td>Event Notes</td>
                <td><?= $event_details['eventMessage']; ?></td>
            </tr>
        </table>
        <?php

    }, 'order', 'normal');
});