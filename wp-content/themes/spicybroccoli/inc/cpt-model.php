<?php
add_action( 'init', 'register_cpt_model' );

function register_cpt_model() {

    $labels = array( 
        'name' => _x( 'Models', 'model' ),
        'singular_name' => _x( 'Model', 'model' ),
        'add_new' => _x( 'Add New', 'model' ),
        'add_new_item' => _x( 'Add New Model', 'model' ),
        'edit_item' => _x( 'Edit Model', 'model' ),
        'new_item' => _x( 'New Model', 'model' ),
        'view_item' => _x( 'View Model', 'model' ),
        'search_items' => _x( 'Search Models', 'model' ),
        'not_found' => _x( 'No models found', 'model' ),
        'not_found_in_trash' => _x( 'No models found in Trash', 'model' ),
        'parent_item_colon' => _x( 'Parent Model:', 'model' ),
        'menu_name' => _x( 'Models', 'model' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon' => 'dashicons-universal-access',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array('slug' => 'models', 'with_front' => false),
        'capability_type' => 'post'
    );

    register_post_type( 'model', $args );
}

class SBM_Model_Metabox {

    public function __construct() {
            add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
            add_action( 'save_post', array( $this, 'save' ) );
       
        }

        /**
         * Adds the meta box container.
         */
        public function add_meta_box( $post_type ) {
            add_meta_box( 'modelmetadiv', 'Model Details', array( $this, 'render_meta_box_content' ), 'model', 'advanced', 'high');
                
        }

        /**
         * Save the meta when the post is saved.
         *
         * @param int $post_id The ID of the post being saved.
         */
        public function save( $post_id ) {
        
            /*
             * We need to verify this came from the our screen and with proper authorization,
             * because save_post can be triggered at other times.
             */

            // Check if our nonce is set.
            if ( ! isset( $_POST['sbm_model_nonce'] ) )
                return $post_id;

            $nonce = $_POST['sbm_model_nonce'];

            // Verify that the nonce is valid.
            if ( ! wp_verify_nonce( $nonce, 'sbm_model' ) )
                return $post_id;

            // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
            if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                return $post_id;

            // Check the user's permissions.
            if ( 'model' == $_POST['post_type'] ) {

                if ( ! current_user_can( 'edit_page', $post_id ) )
                    return $post_id;
        
            } else {

                if ( ! current_user_can( 'edit_post', $post_id ) )
                    return $post_id;
            }

            /* OK, its safe for us to save the data now. */

            // Sanitize the user input.



            // Update the meta field.
            update_post_meta( $post_id, '_model_gender', sanitize_text_field( $_POST['model_gender']) );
            update_post_meta( $post_id, '_model_type',  $_POST['model_type'] );

      

        }


        /**
         * Render Meta Box content.
         *
         * @param WP_Post $post The post object.
         */
        public function render_meta_box_content( $post ) {

        
            // Add an nonce field so we can check for it later.
            wp_nonce_field( 'sbm_model', 'sbm_model_nonce' );

            // Display the form, using the current value.
            echo '<div class="row-container clearfix">';
            $this->checkbox_field(
                array(
                    'key' => 'model_type', 
                    'heading' => 'Model Type', 
                    'options' => array(
                        array('label' => 'Model', 'value' => 'model'),
                        array('label' => 'Stripper', 'value' => 'stripper'),
                    )
                )
            );
            $this->radio_field(
                array(
                    'key' => 'model_gender', 
                    'heading' => 'Gender', 
                    'options' => array(
                        array('label' => 'Male', 'value' => 'male'),
                        array('label' => 'Female', 'value' => 'female'),
                    )
                )
            );
            echo '</div>';
        }

        private function checkbox_field( $opts ) {
            global $post;
            $key = sanitize_title($opts['key']); 
            $value = (array) ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : '';
      
            ?>

            <div class="row row-<?php echo $key; ?>">
                <label for="<?php echo $key; ?>"><?php echo $opts['heading']; ?></label>
                <?php foreach( $opts['options'] as $option ): ?>
                    <p>
                    <input type="checkbox" name="<?php echo $key; ?>[]" value="<?php echo $option['value']; ?>" <?php echo ( in_array($option['value'], (array) $value) ? 'checked="checked"' : '' ); ?>  />
                    <?php echo $option['label']; ?>
                    </p>
                <?php endforeach; ?>
            </div>

        <?php }


        private function radio_field( $opts ) {
            global $post;
            $key = sanitize_title($opts['key']); 
            $value = (array) ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : '';
        
            ?>

            <div class="row row-<?php echo $key; ?>">
                <label for="<?php echo $key; ?>"><?php echo $opts['heading']; ?></label>
                <?php foreach( $opts['options'] as $option ): ?>
                    <p>
                    <input type="radio" name="<?php echo $key; ?>" value="<?php echo $option['value']; ?>" <?php echo ( in_array($option['value'], (array) $value) ? 'checked="checked"' : '' ); ?>  />
                    <?php echo $option['label']; ?>
                    </p>
                <?php endforeach; ?>
            </div>

        <?php }



      private function input_field( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value=" <?php echo esc_attr( trim($value) ); ?>" size="25" />
        </div>

      <?php
        // Reset $value
        $value = null;
      }

      private function textarea_field ( $opts ) { ?>
        <?php 

        global $post;
        $key = sanitize_title($opts['key']); 
        $value = ( get_post_meta($post->ID, '_' . $key, true) ) ? get_post_meta($post->ID, '_' . $key, true) : ''; 

        ?>

        <div class="row row-<?php echo $key; ?>">
          <label for="<?php echo $key; ?>">
            <?php echo $opts['heading']; ?>
          </label>
          <?php wp_editor($value, $key); ?>
        </div>

      <?php 
        // Reset $value
        $value = null;

      }

    



}

new SBM_Model_Metabox();