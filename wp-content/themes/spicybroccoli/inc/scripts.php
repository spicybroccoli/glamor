<?php
/**
 * Load Our Scripts
 *
 */

// Load Our Responsive Nav Scripts
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style('responsive-nav-css', get_stylesheet_directory_uri() .'/css/responsive-nav/responsive-nav.css');
	wp_enqueue_script('jquery');
	wp_enqueue_script('classie', get_stylesheet_directory_uri() . '/js/responsive-nav/classie.js');
	wp_enqueue_script('mlpushmenu', get_stylesheet_directory_uri() . '/js/responsive-nav/mlpushmenu.js');
	wp_enqueue_script('app', get_stylesheet_directory_uri() . '/js/app.js');


	if ( is_singular('model') || is_singular('product') ) {
		wp_enqueue_script('owl', get_stylesheet_directory_uri() . '/js/owl.carousel.min.js', array('jquery'));
		wp_enqueue_style('owl.transitions', get_stylesheet_directory_uri() .'/js/owl.transitions.css');
		wp_enqueue_style('owl.theme', get_stylesheet_directory_uri() .'/js/owl.theme.css');
		wp_enqueue_style('owl.carousel', get_stylesheet_directory_uri() .'/js/owl.carousel.css');
	}

});
