<?php
add_action( 'init', 'register_cpt_location' );

function register_cpt_location() {

    $labels = array( 
        'name' => _x( 'Locations', 'location' ),
        'singular_name' => _x( 'Location', 'location' ),
        'add_new' => _x( 'Add New', 'location' ),
        'add_new_item' => _x( 'Add New Location', 'location' ),
        'edit_item' => _x( 'Edit Location', 'location' ),
        'new_item' => _x( 'New Location', 'location' ),
        'view_item' => _x( 'View Location', 'location' ),
        'search_items' => _x( 'Search Locations', 'location' ),
        'not_found' => _x( 'No locations found', 'location' ),
        'not_found_in_trash' => _x( 'No locations found in Trash', 'location' ),
        'parent_item_colon' => _x( 'Parent Location:', 'location' ),
        'menu_name' => _x( 'Locations', 'location' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        'menu_icon' => 'dashicons-location',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'location', $args );
}