<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


	<section id="footer" class="panel panel__black SiteFooter" role="contentinfo">
		<div class="wrapper">
			<div class="SiteFooter--menu cf">
				<?php wp_nav_menu( array( 'container_class' => 'menu-footer', 'theme_location' => 'footer' ) ); ?>
				<div class="meta">
					<span class="phone">Call: 1300 999 999</span>
					<strong>Mobile</strong><br>
					0414 708 924
					<br><br>
					<strong>Email</strong><br>
					<a href="#">info@glamor.com.au</a>
					<br><br>

					<strong>Opening Hours</strong>
					<br>10:00 AM – 6:00 PM
					<br>Monday-Saturday
				</div>
				
			</div>

			<div class="SiteFooter--colophon cf">
				&copy; <?php echo date("Y"); ?> <?php bloginfo( 'name' ); ?>. All Rights Reserved.  | <a href="<?php echo site_url(); ?>/employment">Employment</a> | <a href="<?php echo site_url(); ?>/testimonials">Testimonials</a> | <a href="<?php echo site_url(); ?>/blog">Blog</a> | Website Design by <a href="http://www.spicybroccoli.com" target="_blank">Spicy Broccoli Media</a>
			</div>

		</div><!-- .wrapper -->
	</section><!-- #footer -->



<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();


	$models_query = new WP_Query(array('post_type' => 'model', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC'));
	$models = array(); 
	foreach ($models_query->posts as $modelInstance) {
	  $connected_services = new WP_Query( array(
	    'connected_type' => 'model_to_product',
	    'connected_items' => $modelInstance,
	    'nopaging' => true,
	  ) );

	  $connected_services = array_map(function($n){
	    return $n->post_title;
	  }, $connected_services->posts);

	  $connected_locations = new WP_Query( array(
	    'connected_type' => 'model_to_location',
	    'connected_items' => $modelInstance,
	    'nopaging' => true,
	  ) );

	  $connected_locations = array_map(function($n){
	    return $n->post_title;
	  }, $connected_locations->posts);

	  $thumb_id = get_post_thumbnail_id($modelInstance->ID);
	  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail', true);
	  $thumb_url = $thumb_url_array[0];

	  $modelsArray = array(
	    'id' => $modelInstance->ID,
	    'name' => $modelInstance->post_title,
	    'location' => $connected_locations,
	    'image' => $thumb_url,
	    'type' => getMetaValues(get_post_meta($modelInstance->ID, 'model-type', true)),
	    'services' => $connected_services,
	  );
	  $models[] = $modelsArray;
	}
	echo '<script>window.models =' . json_encode($models) . '; </script>';
	
	// {id: 4, name: 'Service 4', cost: 401, location: ['Wollongong', 'Sydney'], service_category: ['Cat 1', 'Cat 2']}
	$services_query = new WP_Query(array('post_type' => 'product', 'posts_per_page' => -1));
	$services = array(); 
	foreach ($services_query->posts as $serviceInstance) {
	  $connected_prices = new WP_Query( array(
	    'connected_type' => 'product_to_location',
	    'connected_items' => $serviceInstance,
	    'nopaging' => true,
	  ) );

	  $locations = array_map(function($item){
	    return $item->post_title;
	  }, $connected_prices->posts);

	  $prices = array_reduce($connected_prices->posts, function($carry, $item){
	    $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'cost', true );
	    return $carry;
	  }, array());

	  $deposit = array_reduce($connected_prices->posts, function($carry, $item){
	    $carry[$item->post_title] = p2p_get_meta( $item->p2p_id, 'deposit_value', true );
	    return $carry;
	  }, array());
	  
	  $servicesArray = array(
	    'id' => $serviceInstance->ID,
	    'name' => $serviceInstance->post_title,
	    'cost' => $prices,
	    'locations' => $locations,
	    'description' => wpautop($serviceInstance->post_content),
	    'pricingType' => get_post_meta($serviceInstance->ID, 'pricing-type', true),
	    'serviceCategory' => getMetaValues(get_post_meta($serviceInstance->ID, 'product-type', true)),
	    'minDuration' => (int) get_post_meta($serviceInstance->ID, 'min-duration', true),
	    'maxDuration' => (int) get_post_meta($serviceInstance->ID, 'max-duration', true),
	    'deposit' => $deposit,
	    'partyPackage' => (get_post_meta($serviceInstance->ID, 'party-package', true) == 'Yes') ? true : false
	  );
	  $services[] = $servicesArray;
	}

	echo '<script>window.services =' . json_encode($services) . '; </script>';

	$cities = new WP_Query(array('post_type' => 'location', 'posts_per_page' => -1));

  	$citiesValues = array_map(function($item){
    	return array('id' => $item->ID, 'name' => $item->post_title);
  	}, $cities->posts);

	echo '<script>window.cities =' . json_encode($citiesValues) . '; </script>';
	
	if (is_singular('express_orders')) {
		global $post;
		if (get_post_meta($post->ID, 'city', true)) {
		    echo '<script> window.currentCity = "' . get_post_meta($post->ID, 'city', true) . '"; </script>';
		}
		
		if (get_post_meta($post->ID, 'cart_data', true)) {
		    echo '<script>window.initialCart =' . json_encode(array_unique(get_post_meta($post->ID, 'cart_data', true), SORT_REGULAR)) . '; </script>';
		} else {
		    echo '<script>window.initialCart = [];</script>';
		}

	} else {
		global $session_cart;
		
		if (GS_Session::get_city()) {
		    echo '<script> window.currentCity = "' . GS_Session::get_city() . '"; </script>';
		}
		
		if ($session_cart['cart_data']) {
		    echo '<script>window.initialCart =' . json_encode(array_unique($session_cart['cart_data'], SORT_REGULAR)) . '; </script>';
		} else {
		    echo '<script>window.initialCart = [];</script>';
		}
	}

	
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js"></script>
	<script>
	if ('addEventListener' in document) {
	    document.addEventListener('DOMContentLoaded', function() {
	        FastClick.attach(document.body);
	    }, false);
	}
	</script>
	<script>window.GS_ajaxurl = '<?= admin_url('/admin-ajax.php?action=sync'); ?>'; </script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/js/DateTimePicker.min.css" />
	 <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/DateTimePicker.min.js"></script>
	 <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/responsive-tables.js"></script>
<script>	new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/parsley.js/2.0.7/parsley.min.js"></script>
</body>
</html>