<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel">
		<div class="wrapper cf">
			<div class="PageContent">
				<div class="PageContent--title">
					<h1>Party Packages</h1>
					<p>These are the party packages available for the location you have selected. Click to on a package below to find out more.</p>

				</div><!-- .PageContent--title -->

				<div class="PageContent--Location">
					<p>To update your location choose another city below.</p>
					<?php GS_Session::city_form(); ?>
				</div><!-- .PageContent--Location -->

				<div class="PageContent--Packages">
				<?php if(have_posts()): ?>
					<?php while(have_posts()): the_post(); ?>
						<?php
						global $session_cart;
						$cart = $session_cart['cart_data'];
						$itemInCart = false;

						if (!$cart) {
							$cart = array();
						}
						
						if (count($cart) > 0) {
							$cartItem = array_values(array_filter($cart, function($cartItem){
								return ($cartItem['id'] == get_the_ID());
							}));
							if (count($cartItem) > 0) {
								$itemInCart = true;
							}

						}
						$action = ($itemInCart) ? 'removeFromCart' : 'addToCart';

						?>

						<?php global $post; ?>
					<div class="PageContent--Packages--Item">
						<h3>Angels</h3>
						<div class="ItemContent">
							<span class="price">$<?php echo p2p_get_meta( $post->p2p_id, 'cost', true ); ?></span>
							<img src="http://dev.spicybroccolitesting.com.au/glamor/wp-content/themes/spicybroccoli/images/package_thumbnail.jpg" alt="">
							<div class="ItemContent--Copy">
								<h4><?php the_title(); ?></h4>
								<?php the_content(); ?>
								<a href="<?php the_permalink(); ?>" class="button">View Package</a>
							</div><!-- .ItemContent--Copy -->
						</div><!-- .ItemContent -->
					</div><!-- .PageContent--Packages--Item -->
					<?php endwhile; ?>
				<?php else: ?>
					<p>Sorry no party packages are available in your area</p>
				<?php endif; ?>
				
				</div><!-- .PageContent--Packages -->
				
			</div><!-- .PageContent -->
		</div><!-- .wrapper -->
	</section><!-- .panel -->
	
	<?php GS_Session::city_form_modal(); ?>

<?php get_footer(); ?>