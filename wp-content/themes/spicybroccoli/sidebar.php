<span>Recent Posts</span>
	<?php $recent_posts = new WP_Query(array(
		'post_type' => 'post',
		'posts_per_page' => 3,
		'post__not_in' => array(get_the_ID())
		));
	?>
<ul>
	<?php while($recent_posts->have_posts()): $recent_posts->the_post(); ?>
			<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
	<?php endwhile; ?>
</ul>
<span>Archive</span>
	<ul>
		<?php wp_get_archives(); ?>
	</ul>