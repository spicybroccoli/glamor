<?php get_header(); ?>

  <section class="panel">
    <div class="wrapper cf">
      <div class="PageContent">
        <div class="PageContent--title">
          <h1>Page Not Found</h1>
          <p>Sorry the page you requested could not be found.</p>
          <p>Follow our simple 3 step process to secure your booking.</p>
          <p><a class="button" style="margin:10px auto;" href="<?= site_url('/book-online'); ?>">Book Online</a></p>
        </div><!-- .PageContent--title -->
      </div>
      
  </section>
<?php get_footer(); ?>