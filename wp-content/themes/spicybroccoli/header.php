<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_stylesheet_directory_uri(); ?>/style-responsive.css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="mp-pusher" class="mp-pusher">

    <nav class="mp-menu" id="mp-menu">
		<div class="mp-level">    	
	        <?php
	           	wp_nav_menu( 
	            	array( 
	            		'menu' => 'main-nav',
	            		'container'  => 'false',
	            		'container_id' => 'mp-menu',
	            		'menu_class' => 'mp-level',
	            		'container_class' => 'mp-menu',
	            		'menu_class' => '',  
	            		'walker' => new SBM_Responsive_Nav()
	            	) 
	            ); 
	        ?>	
                    
        </div>
    </nav>

    <div id="top-menu">
		<a href="#" id="trigger" class="menu-trigger">Open/Close Menu</a>

	</div>
	
	<section id="header" class="panel SiteHeader <?php echo ( !is_front_page() ) ? 'SiteHeader__page' : ''; ?>">
		<div class="wrapper">
			<div class="SiteHeader--location PageContent--Location">
				<?php GS_Session::city_form(); ?>
			</div><!-- .SiteHeader--location -->

			<div class="SiteHeader--meta">
				<a class="SiteHeader--meta--cart" href="<?php echo site_url('/cart'); ?>"><span class="fa fa-shopping-cart"></span></a>
				<a class="SiteHeader--meta--wishlist" href="<?php echo site_url('/cart'); ?>"><span class="fa fa-heart"></span></a>
				<span class="SiteHeader--meta--phone">1300 933 922</span>
				<form action="<?= site_url('/'); ?>"><input type="search" name="s" placeholder="Search..." value="<?= get_search_query(); ?>"></form>
			</div>
			<div class="SiteHeader--logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png"></a>
			</div>
		</div><!-- .wrapper-->
		<div class="SiteHeader--menu">
			<div class="wrapper">
				<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
			</div><!-- .wrapper -->
		</div>
		<?php if ( is_front_page() || is_home() ) { ?>
		<div class="SiteHeader--hero">
			<?php echo do_shortcode("[metaslider id=26]"); ?>
		</div>
		<?php } ?>
	</section><!-- #header -->