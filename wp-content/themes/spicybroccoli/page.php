<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

<section class="panel">
	<div class="wrapper cf">
		<div class="PageContent">
			<div class="PageContent--title">
				<h1><?php the_title(); ?></h1>
			</div>
			<div class="PageContent--content">
				
				<?php while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content( 'Continue reading <span class="meta-nav">&rarr;</span>'); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-link">' .  'Pages:' , 'after' => '</div>' ) ); ?>
						</div><!-- .entry-content -->
					</div><!-- #post-## -->
				<?php endwhile; // End the loop. Whew. ?>
			</div>
		</div><!-- .PageContent -->
	</div><!-- .wrapper -->
</section>

<?php get_footer(); ?>