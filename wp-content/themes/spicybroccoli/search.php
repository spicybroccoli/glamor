<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel">
		<div class="wrapper cf">
			<div class="PageContent">
			
				<div class="PageContent--title">
					<h1>Search results for: <?= get_search_query(); ?></h1>
					<p>These are the search results available for the location you have selected. Click on each item to view more.</p>

				</div><!-- .PageContent--title -->

				<div class="PageContent--Location">
					<p>To update your location choose another city below.</p>
					<?php GS_Session::city_form(); ?>
				</div><!-- .PageContent--Location -->


				<div class="PageContent--Packages PageContent--Packages__Services cf">

						<?php if (have_posts()): ?>
							<div class="PageContent--Packages--Item PageContent--Packages--Item__Services cf">
								<?php while(have_posts()): the_post(); ?>
									
								<div class="ItemContent ItemContent__Services cf">
									<?php the_post_thumbnail(); ?>
									<div class="ItemContent--Copy">
										<h4><?php the_title(); ?></h4>
										<?php the_excerpt(); ?>
										<a href="<?php the_permalink(); ?>" class="button">View More</a>
									</div><!-- .ItemContent--Copy -->
								</div><!-- .ItemContent -->
								
							<?php endwhile; ?>
							</div>
						<?php else: ?>
							<p>Sorry we couldn't find anything that matched <?= get_search_query(); ?></p>
						<?php endif; ?>
						</div><!-- .PageContent--Packages--Item -->
		
				</div><!-- .PageContent--Packages -->
				
			</div><!-- .PageContent -->
		</div><!-- .wrapper -->
	</section><!-- .panel -->

	<?php GS_Session::city_form_modal(); ?>

<?php get_footer(); ?>