<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel">
		<div class="wrapper cf">
			<div class="PageContent">
				
					<?php do_action('notifications'); ?>
				
			
			<?php while(have_posts()): the_post(); ?>
				<div class="PageContent--title">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div><!-- .PageContent--title -->
				
				<div class="PageContent--ModelProfile cf">
					<div class="PageContent--ModelProfile--HeroImage">
						<?php
							$galleryArray = get_post_gallery_ids(get_the_ID()); 

						?>
						<?php foreach ($galleryArray as $id): ?>
						<div class="item">
							<a href="#"><img src="<?php echo wp_get_attachment_url( $id ); ?>" alt=""></a>
						</div>
						<?php endforeach; ?>
					</div><!-- .PageContent--ModelProfile--HeroImage -->
					<div class="PageContent--ModelProfile--Services">
					<h3>Services available</h3>
						<?php
						$model_id = get_the_ID();
						$location_args = array(
						  'connected_type' => 'model_to_location',
						  'connected_items' => get_the_ID(),
						  'nopaging' => true
						);

						$connected_locations = new WP_Query($location_args);

						$locations = array_map(function($post_object){
							return $post_object->post_title;
						}, $connected_locations->posts);

						if (in_array(GS_Session::get_city(), $locations)) {



							$services_args = array(
							  'connected_type' => 'model_to_product',
							  'connected_items' => get_the_ID(),
							  'nopaging' => true
							);

							if (GS_Session::get_city()) {
								$services_args['connected_items'] = array('model_to_product');
								$id =  get_page_by_path( get_city(), OBJECT, 'location');

								$services_args['connected_items'] = array(get_the_ID());
								$services_args['post_type'] = 'product';
							}

							$connected_services = new WP_Query($services_args); ?>


						

							<?php if ( $connected_services->have_posts() ) : ?>
							
							<ul class="ServicesList">
							<?php while ( $connected_services->have_posts() ) : $connected_services->the_post(); ?>

						


								<?php
									global $session_cart;
									$cart = $session_cart['cart_data'];
									$modelInItem = false;
									if (count($cart) > 0) {
										$cartItem = array_values(array_filter($cart, function($cartItem){
											return ($cartItem['id'] == get_the_ID());
										}));
										if (count($cartItem) > 0) {
											if ($cartItem[0]['models']) {
												if (count($cartItem[0]['models']) > 0) {

													$modelItem = array_values(array_filter($cartItem[0]['models'], function($item) use ($model_id) {
														return ($item['id'] == $model_id);
													}));

													if (count($modelItem) > 0) {
														$modelInItem = true;
													}
												}
											}
											
											
										}

									}
									$action = ($modelInItem) ? 'removeFromCart' : 'addToCart';
								?>
								<li><?php the_title(); ?>
								<a href="<?php echo add_query_arg(array($action => 1, 'model_id' => $model_id, 'service_id' => get_the_ID())); ?>" class="<?php echo ($modelInItem) ? 'RemoveFromWishlist' : 'AddToWishlist'; ?>">
									<span class="fa fa-heart"></span>
									<span data-remove-label="Remove From Wishlist" data-add-label="Add to Wishlist" class="AddToWishlist--label">
									<?php echo ($modelInItem) ? 'Remove From Cart' : 'Add To Cart'; ?>
									</span>
								</a>
								</li>
							<?php endwhile; ?>
							</ul>
							<?php endif; ?>
							<?php wp_reset_postdata(); wp_reset_query(); 
							} else { ?>
							<p>Sorry, there are no services available in your city</p>
							<?php } ?>
					</div><!-- .PageContent--ModelProfile--Services -->
					<div class="PageContent--ModelProfile--Gallery">
						<?php
							$galleryArray = get_post_gallery_ids(get_the_ID()); 

						?>
						<h3>Photo Gallery</h3>
						<ul class="GalleryList">
							<?php foreach ($galleryArray as $id): ?>
							<li class="item">
								<img src="<?php echo wp_get_attachment_thumb_url( $id ); ?>" alt=""/>
							</li>
							<?php endforeach; ?>
						</ul>
					</div><!-- .PageContent--ModelProfile--Gallery -->
				</div><!-- .PageContent--ModelProfile -->
		
				<?php if (GS_Session::get_city()): ?>
					<?php 
						
						$query = array(
							'post_type' => 'model',
							'posts_per_page' => 5,
							'orderby' => 'menu_order',
							'order' => 'ASC'
							);
		

					
					  $id =  get_page_by_path( GS_Session::get_city(), $output, 'location');
					  $query['connected_type'] = 'model_to_location';
					  $query['connected_items'] = $id->ID;
					  $query['connected_direction'] = 'to';
					

					$models_query = new WP_Query($query); ?>

					
					<?php if ($models_query->have_posts()): ?>
						<div class="PageContent--OtherGirls">
							<h2>Other Girls in your Area</h2>


					<ul class="ModelList cf">
						<?php while($models_query->have_posts()): $models_query->the_post(); ?>
						<li><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?><span class="ModelListItem--Name"><?php the_title(); ?></span><div class="ModelListItem--Overlay"><span>View Profile</span></div></a></li>
						<?php endwhile; ?>
					</ul><!-- .ModelList -->
					
				</div><!-- .PageContent--OtherGirls -->
					<?php endif; ?>
			<?php endif; ?>
				
			</div><!-- .PageContent -->
			<?php endwhile; ?>
		</div><!-- .wrapper -->
	</section><!-- .panel -->


	<script>
	jQuery(document).ready(function($) {
	 
	  var sync1 = $(".PageContent--ModelProfile--HeroImage");
	  var sync2 = $(".GalleryList");
	 
	  sync1.owlCarousel({
	    singleItem : true,
	    slideSpeed : 1000,
	    navigation: true,
	    pagination:false,
	    transitionStyle : "fade",
	    afterAction : syncPosition,
	    responsiveRefreshRate : 200,
	  });
	 
	  sync2.owlCarousel({
	    items : 3,
	    pagination:false,
	    responsiveRefreshRate : 100,
	    afterInit : function(el){
	      el.find(".owl-item").eq(0).addClass("synced");
	    }
	  });
	 
	  function syncPosition(el){
	    var current = this.currentItem;
	    sync2
	      .find(".owl-item")
	      .removeClass("synced")
	      .eq(current)
	      .addClass("synced")
	    if(sync2.data("owlCarousel") !== undefined){
	      center(current)
	    }
	  }
	 
	  sync2.on("click", ".owl-item", function(e){
	    e.preventDefault();
	    var number = $(this).data("owlItem");
	    sync1.trigger("owl.goTo",number);
	  });
	 
	  function center(number){
	    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
	    var num = number;
	    var found = false;
	    for(var i in sync2visible){
	      if(num === sync2visible[i]){
	        var found = true;
	      }
	    }
	 
	    if(found===false){
	      if(num>sync2visible[sync2visible.length-1]){
	        sync2.trigger("owl.goTo", num - sync2visible.length+2)
	      }else{
	        if(num - 1 === -1){
	          num = 0;
	        }
	        sync2.trigger("owl.goTo", num);
	      }
	    } else if(num === sync2visible[sync2visible.length-1]){
	      sync2.trigger("owl.goTo", sync2visible[1])
	    } else if(num === sync2visible[0]){
	      sync2.trigger("owl.goTo", num-1)
	    }
	    
	  }
	 
	});
	</script>


<section class="panel panel__dark">
	<?php get_template_part('partials/book-online'); ?>
</section>
<section class="panel">
	<?php get_template_part('partials/contact-us'); ?>
</section>

<?php GS_Session::city_form_modal(); ?>

<?php get_footer(); ?>