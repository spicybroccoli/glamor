<div class="wrapper cf">
	<div class="BookOnlineWidget">
		<div class="BookOnlineWidget--title">
			<h2>Book Online</h2>
		</div>
		<div class="BookOnlineWidget--steps">
			<div class="SteppedProgressBar">
				<ul>
			    	<li class="active">
			    		<div class="step-text"><strong>Step 1</strong>Choose Location</div>
			    		<span class="arrow-background"><span class="arrow-wrapper"><span class="arrow"></span></span></span></li>
			    	<li>
						<div class="step-text"><strong>Step 2</strong>Choose Show</div>
			    		<span class="arrow-background"><span class="arrow-wrapper"><span class="arrow"></span></span></span></li>
			    	<li>
						<div class="step-text"><strong>Step 3</strong>Choose Girls</div>
			    	</li>
		    	</ul>
		    </div><!-- .SteppedProgressBar -->
		</div>

		<div class="BookOnlineWidget--form">
			<?php 
				$current_city = GS_Session::get_city();
				$cities = new WP_Query(array('post_type' => 'location', 'posts_per_page' => -1, 'orderby' => 'menu_order', 'order' => 'ASC'));
			?>
			<form method="post" class="session-city" action="<?php echo site_url('/cart/update'); ?>">
				<input type="hidden" value="<?php echo site_url('/book-online'); ?>" name="referrer">
				<select name="city">
					<option>Choose your City</option>
					<?php foreach($cities->posts as $city): ?>
						<option value="<?php echo $city->post_title; ?>" <?php echo ($current_city == $city->post_title) ? 'selected="selected"' : ''; ?>><?php echo $city->post_title; ?></option>
					<?php endforeach; ?>
				</select>
			</form>
		</div>
	</div>
</div>