<div class="wrapper cf">
	<ul class="CTABlockWrapper cf">

		<li class="CTABlock">
			<div class="CTABlock--title"><h3><a href="<?php echo site_url('/strippers'); ?>">Strippers</a></h3></div>
			<a href="<?php echo site_url('/strippers'); ?>" class="CTABlock--FeatImg">
				<img src="<?= site_url('/wp-content/uploads/2015/02/cta-block-1.jpg'); ?>" alt="">
				<div class="CTABlock--FeatImg--Overlay">
					<span>See all<br/>Strippers</span>
				</div>
			</a>
		</li><!-- .CTABlock -->

		<li class="CTABlock">
			<div class="CTABlock--title"><h3><a href="<?php echo site_url('/models'); ?>">Models</a></h3></div>
			<a href="<?php echo site_url('/models'); ?>" class="CTABlock--FeatImg">
				<img src="<?= site_url('/wp-content/uploads/2015/02/cta-block-1.jpg'); ?>" alt="">
				<div class="CTABlock--FeatImg--Overlay">
					<span>See all<br/>Models</span>
				</div>
			</a>
		</li><!-- .CTABlock -->

		<li class="CTABlock">
			<div class="CTABlock--title"><h3><a href="<?php echo site_url('/party-packages'); ?>">Party Packages</a></h3></div>
			<a href="<?php echo site_url('/party-packages'); ?>" class="CTABlock--FeatImg">
				<img src="<?= site_url('/wp-content/uploads/2015/02/cta-block-1.jpg'); ?>" alt="">
				<div class="CTABlock--FeatImg--Overlay">
					<span>See all<br/>Party Packages</span>
				</div>
			</a>
		</li><!-- .CTABlock -->

		<li class="CTABlock">
			<div class="CTABlock--title"><h3><a href="<?php echo site_url('/services'); ?>">Our Services</a></h3></div>
			<a href="<?php echo site_url('/services'); ?>" class="CTABlock--FeatImg">
				<img src="<?= site_url('/wp-content/uploads/2015/02/cta-block-1.jpg'); ?>" alt="">
				<div class="CTABlock--FeatImg--Overlay">
					<span>See all<br/>Services</span>
				</div>
			</a>
		</li><!-- .CTABlock -->

		<li class="CTABlock">
			<div class="CTABlock--title"><h3><a href="<?php echo site_url('/book-online'); ?>">Book Online</a></h3></div>
			<a href="<?php echo site_url('/book-online'); ?>" class="CTABlock--FeatImg">
				<img src="<?= site_url('/wp-content/uploads/2015/02/cta-block-1.jpg'); ?>" alt="">
				<div class="CTABlock--FeatImg--Overlay">
					<span>See<br/>Our Rates</span>
				</div>
			</a>
		</li><!-- .CTABlock -->

	</ul><!-- .CTABlockWrapper -->
</div>