<?php get_header(); ?>

<?php get_header(); ?>

  <section class="panel">
    <div class="wrapper cf">
      <div class="PageContent">
        <div class="PageContent--title">
          <h1><?php the_title(); ?></h1>
        </div><!-- .PageContent--title -->
        <div id="order-checkout"></div>
        <div id="checkout-actions"></div>
    </div>
  </section>

<script src="<?php echo get_stylesheet_directory_uri();?>/js/cart/checkout.js"></script>

<?php get_footer(); ?>