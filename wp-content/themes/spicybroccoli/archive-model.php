<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

	<section class="panel">
		<div class="wrapper cf">
			<div class="PageContent">
				<div class="PageContent--title">
	
					<h1><?php echo (get_query_var('model_archive') == 'stripper') ? 'Strippers' : 'Models'; ?></h1>
					<p>These are the models available for the location you have selected. Please click to view their profile.</p>
				</div><!-- .PageContent--title -->

				<div class="PageContent--Location">
					<p>To update your location choose another city below.</p>
					<?php GS_Session::city_form(); ?>
				</div><!-- .PageContent--Location -->

				<div class="PageContent--ModelList">
					

					<?php if (have_posts()): ?>
						<div class="PageContent--Packages--Item">
							<h3>
								<?= ucfirst(get_query_var('model_gender')); ?>
								<?= (get_query_var('model_archive') == 'stripper') ? 'Strippers' : 'Models'; ?>
							</h3>
						</div>
						
					<ul class="ModelList cf">
						<?php while(have_posts()): the_post(); ?>
							
						<li>
							<a href="<?php the_permalink(); ?>">
								<?php the_post_thumbnail(); ?>
								<span class="ModelListItem--Name"><?php the_title(); ?></span>
								<div class="ModelListItem--Overlay"><span>View Profile</span></div>
							</a>
						</li>
						<?php endwhile; ?>
					</ul><!-- .ModelList -->
		
					</div>
					<?php else: ?>
						<p>Sorry no <?php echo (get_query_var('model_archive') == 'stripper') ? 'strippers' : 'models'; ?> are available in your area</p>
					<?php endif; ?>
					<!-- <div class="pagination">
						<ul class="pagination--numbers"><li><a class="prev" href="#">Prev</a></li><li><a href="#">1</a></li><li><span class="current">2</span></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><span class="dots">…</span></li><li><a href="#">17</a></li><li><a href="#">18</a></li><li><a class="next" href="#">Next</a></li>
						</ul>
					</div><!-- .pagination -->
				</div><!-- .PageContent--ModelList -->
				
			</div><!-- .PageContent -->
		</div><!-- .wrapper -->
	</section><!-- .panel -->

	<?php GS_Session::city_form_modal(); ?>


<?php get_footer(); ?>