<?php
/**
 * Theme Functions
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */

if ( ! isset( $content_width ) )
	$content_width = 640;

/** Tell WordPress to run twentyten_setup() when the 'after_setup_theme' hook is run. */
add_action( 'after_setup_theme', function() {

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
			'primary' => 'Primary Navigation',
            'footer' => 'Footer Navigation',
		) );

});

// Bootstrap Theme
include('inc/template-utils.php'); // Load Template Utilities
include('inc/branding.php'); // Load Template Utilities
include('inc/responsive-nav.php'); // Responsive Nav
// include('inc/landing.php'); // Load Landing page if necessary
include('inc/scripts.php'); // Load Scripts
include('inc/routes.php');
include('inc/cpt-model.php');
include('inc/cpt-product.php');
include('inc/cpt-location.php');
include('inc/cpt-order.php');
include('inc/tax-product-category.php');

// Session related classes
// Note - WP is stateless, using a 'session cookie' 
// - a cookie that refers to the key of a WP Transient
include('inc/session.php');

//
include('inc/express-checkout-cpt.php');

add_action( 'p2p_init', 'my_connection_types' );

function my_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'model_to_product',
        'from' => 'model',
        'to' => 'product'
    ) );
    p2p_register_connection_type( array(
        'name' => 'model_to_location',
        'from' => 'model',
        'to' => 'location'
    ) );
    p2p_register_connection_type( array(
        'name' => 'product_to_location',
        'from' => 'product',
        'to' => 'location',
        'fields' => array(
                'cost' => array(
                    'title' => 'Cost ($)',
                    'type' => 'text',
                ),
                'deposit_value' => array (
                    'title' => 'Deposit ($)',
                    'type' => 'text'
                )
        )
    ) );
}

function my_admin_theme_style() {
    wp_enqueue_style('admin', get_stylesheet_directory_uri() . '/inc/admin.css');
}
add_action('admin_enqueue_scripts', 'my_admin_theme_style');

add_filter('fg_post_types', function(){
	return array('model', 'product');
});

  function getMetaValues($object) {
    $array = array();
    if (is_array($object)) {
      foreach ($object as $key => $value) {
        if ($key !== '__force_post') {
          $array[] = $value;
        }
      }
    }
    return $array;
  }

  if ( !class_exists( 'GS_Menu' ) ) {
      class GS_Menu {
          public function hijack_menu($objects) {
            /**
         * If user isn't logged in, we return the link as normal
         */
        $current_url = 'http://dev.spicybroccolitesting.com.au' . $_SERVER['REQUEST_URI'];
        /**
         * If they are logged in, we search through the objects for items with the 
         * class wl-login-pop and we change the text and url into a logout link
         */
        foreach ( $objects as $k=>$object ) {

    

            if ( $object->url == $current_url ) {

     

                $object->classes[] = 'current_page_item';
            }
        
        }
   
        return $objects;
    }
      }
  }
   
  $hijackme = new GS_Menu;
   
  add_filter('wp_nav_menu_objects', array($hijackme, 'hijack_menu'), 10, 2);


  function mySearchFilter($query) {

    if ($query->is_search) {
        $query->set('post_type', array('model', 'product', 'page', 'post') );
    };
    return $query;
};

add_filter('pre_get_posts','mySearchFilter');

add_filter( 'body_class', 'my_class_names' );
function my_class_names( $classes ) {
  if (is_singular('express_orders')) {
    $classes[] = 'page-cart';
  }
  // return the $classes array
  return $classes;
}