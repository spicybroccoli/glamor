<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'db142268_glamor');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'db142268');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'fingered!');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'internal-db.s142268.gridserver.com');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}
define('WPCF7_AUTOP', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!e4HuA}_+|i {0bhxo0.=6u.O!p!GyF+?V+yz{( p|5mdcl/m%y[S~Uo9Dg|0zP[');
define('SECURE_AUTH_KEY',  'ho$l;p[&gy0+pOcSMSEKn_@*&z7#r-U).`bI!h2`l>d~I,O|_@-LBTA~(Q/?N7|-');
define('LOGGED_IN_KEY',    '`L/fEM0rh%s4tA#bC)vWPIrxmu){im0ga(6ndV% %:cYR+.1FbQ]G3v=9P&<o[4p');
define('NONCE_KEY',        '*cO:C4&610VdAN5JnuJ?2? hRQ  ZP4X4NAKeE:z;ipDA:3A @$4C6wz<o(eL229');
define('AUTH_SALT',        'Ee2a<g0X;-|FvK:m)oJN>KvZhG,i?|<-%{[q66^@rw-9Hx#1MI.tS,}dSnqDZd1]');
define('SECURE_AUTH_SALT', 's0eyk1Xos5g51YnIiO.,3A/2M{W%GrBIY?1=gY+2g[Mqx;[ck2usw-DVj:{-n>|!');
define('LOGGED_IN_SALT',   '_fTWenB+X]@:K$n`cEuvGtl:T?!s+RXleyz5=6^-f<90>!%A).+SjML{I5-*6Sr/');
define('NONCE_SALT',       'w(m#+vS,[9Uf@vl]@K80Zr@ATSLO-h+C8]/s|RBV3)Uw|b&%5ay%/%fo`pg4_%Lq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'sbm_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
